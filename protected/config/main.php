<?php

$base 		 = __DIR__ . '/..';
$common      = __DIR__ . '/../../common';
$environment = APP_ENVIRONMENT;
$params      = require(__DIR__ . '/params.php');

return CMap::mergeArray(require($common . '/config/main.php'), array(
	'components' => array(
		'urlManager' => array(
			'urlFormat' => 'path',
			'showScriptName' => false, // requer .htaccess
			'rules' => array(
				'empresa' => 'home/about',
				'quem-somos' => 'home/about',
				'sobre' => 'home/about',
				'seja-um-franqueado' => 'home/franchise',
				'franquia' => 'home/franchise',
				'produtos' => 'product',
				'pedido/produto/<id>' => 'product/order',
				'produtos/<category>' => 'product/index',
				'produto/<id:\d+>' => 'product/view',
				'lojas' => 'unit',
				'loja/<id:\d+>' => 'unit/view',
				'blog' => 'post',
				'blog/enviar-publicacao' => 'post/submit',
				'blog/<id:\d+>' => 'post/view',
				'blog/<id>' => 'post/view',
				'blog/categoria/<category>' => 'post/index',
			),
		),
	),

	'params' => $params,
), require(__DIR__ . '/main-' . $environment . '.php'));