<?php

class FranchiseContactForm extends CFormModel
{
	public $name;
	public $email;
	public $birthDate;
	public $businessPhone;
	public $mobilePhone;
	public $region;
	public $reason;
	public $message;

	public function rules()
	{
		return array(
			array('name, email', 'required'),
			array('email', 'email'),
			array('birthDate, businessPhone, mobilePhone, region, reason, message', 'safe'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'name' => 'Nome',
			'email' => 'E-mail',
			'birthDate' => 'Data de Nascimento',
			'businessPhone' => 'Telefone Comercial',
			'homePhone' => 'Telefone Residencial',
			'mobilePhone' => 'Telefone Celular',
			'mobilePhone' => 'Telefone Celular',
			'zip' => 'CEP',
			'address' => 'Endereço',
			'complement' => 'Complemento',
			'neighborhood' => 'Bairro',
			'city' => 'Cidade',
			'state' => 'Estado',
			'region' => 'Região',
			'reason' => 'Motivo',
			'message' => 'Mensagem',
		);
	}

	public function getReasonOptions()
	{
		return array(
			1 => 'Dúvidas',
			2 => 'Solicitação de Contato',
			3 => 'Sugestões',
		);
	}

	public function process()
	{
		$message = new YiiMailMessage;
		$message->setTo(Yii::app()->params['franchise.email']);
		$message->setFrom(Yii::app()->params['mail.senderEmail']);
		$message->setReplyTo($this->email);
		$body = Yii::app()->controller->renderPartial(
			'application.views.mail.internal.franchiseContact', 
			array('franchiseContactForm' => $this, 'message' => $message), true
		);	
		$message->setBody(array('message' => $message, 'content' => $body), 'text/html');
		return Yii::app()->mail->send($message);
	}
}