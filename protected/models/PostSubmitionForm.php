<?php

class PostSubmitionForm extends PostSubmition
{
	public $notify_sender = true;

	public static function model($className=__CLASS__)
	{
		return new self;
	}

	public function attributeLabels()
	{
		return CMap::mergeArray(parent::attributeLabels(), array(
			'sender_name' => 'Seu nome',
			'sender_email' => 'Seu e-mail',
			'sender_friends_emails' => 'E-mails dos seus amigos(as)',
		));
	}

	public function behaviors()
	{
		return CMap::mergeArray(parent::behaviors(), array(
			'fileBehavior' => array(
				'uploadModelFolder' => Yii::app()->user->getState(__CLASS__ . '.uploadFolder'),
			),
		));
	}

	protected function beforeValidate()
	{
		if (parent::beforeValidate()) {
			if (count($gallery = $this->getFiles('gallery')) < 1)
				$this->addError('upload_folder', 'Nenhum arquivo enviado.');

			return true;
		} else
			return false;
	}

	protected function afterConstruct()
	{
		parent::afterConstruct();

		if (!Yii::app()->user->hasState(__CLASS__ . '.uploadFolder')) {
			$uploadFolder = substr(md5(uniqid()), 0, 10);
			Yii::app()->user->setState(__CLASS__ . '.uploadFolder', $uploadFolder);
		}

		$this->upload_folder = Yii::app()->user->getState(__CLASS__ . '.uploadFolder'); 
	}

	protected function afterSave()
	{
		parent::afterSave();
		Yii::app()->user->setState(__CLASS__ . '.uploadFolder', null);

		$message = new YiiMailMessage;
		$message->setTo(Yii::app()->params['mail.senderEmail']);
		$message->setFrom(Yii::app()->params['mail.senderEmail']);

		$body = Yii::app()->controller->renderPartial(
			'application.views.mail.internal.postSubmitionCreated', 
			array('postSubmition' => $this, 'message' => $message), true
		);

		$message->setBody(array('message' => $message, 'content' => $body), 'text/html');
		Yii::app()->mail->send($message);
	}
}