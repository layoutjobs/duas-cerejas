<?php

class FranchiseApplicationForm extends CFormModel
{
	public $name;
	public $email;
	public $birthDate;
	public $businessPhone;
	public $homePhone;
	public $mobilePhone;

	public $zip;
	public $address;
	public $complement;
	public $neighborhood;
	public $city;
	public $state;

	public $equityCapital;
	public $region;
	public $businessArea;

	public $whereYouFindUs;
	public $aditionalInformation;

	public function rules()
	{
		return array(
			array('name, email', 'required'),
			array('email', 'email'),
			array('birthDate, businessPhone, homePhone, mobilePhone, zip, address, complement, neighborhood, city, state, equityCapital, region, businessArea, whereYouFindUs, aditionalInformation', 'safe'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'name' => 'Nome',
			'email' => 'E-mail',
			'birthDate' => 'Data de Nascimento',
			'businessPhone' => 'Telefone Comercial',
			'homePhone' => 'Telefone Residencial',
			'mobilePhone' => 'Telefone Celular',
			'mobilePhone' => 'Telefone Celular',

			'zip' => 'CEP',
			'address' => 'Endereço',
			'complement' => 'Complemento',
			'neighborhood' => 'Bairro',
			'city' => 'Cidade',
			'state' => 'Estado',

			'equityCapital' => 'Capital Próprio para Investimento',
			'region' => 'Região de Interesse',
			'businessArea' => 'Ramo de Atividade',

			'whereYouFindUs' => 'Onde nos conheceu?',
			'aditionalInformation' => 'Informações adicionais',
		);
	}

	public function getEquityCapitalOptions()
	{
		return array(
			1 => 'Até R$ 4.999,00',
			2 => 'De R$ 5.000,00 a R$ 9.999,00',
			3 => 'De R$ 10.000,00 a R$ 49.999,00',
			4 => 'Acima de R$ 49.999,00',
		);
	}

	public function getRegionOptions()
	{
		return array(
			1 => 'Campinas',
			2 => 'Sorocaba',
		);
	}

	public function process()
	{
		$message = new YiiMailMessage;
		$message->setTo(Yii::app()->params['franchise.email']);
		$message->setFrom(Yii::app()->params['mail.senderEmail']);
		$message->setReplyTo($this->email);
		$body = Yii::app()->controller->renderPartial(
			'application.views.mail.internal.franchiseApplication', 
			array('franchiseApplicationForm' => $this, 'message' => $message), true
		);	
		$message->setBody(array('message' => $message, 'content' => $body), 'text/html');
		return Yii::app()->mail->send($message);
	}
}