<?php

class OrderForm extends CFormModel
{
	public $product;
	public $unit;
	public $name;
	public $phone;
	public $email;
	public $quantity;
	public $deliveryDate;
	public $notes;
	public $image;

	public function rules()
	{
		return array(
			array('product, unit, name, phone, email, quantity', 'required'),
			array('email', 'email'),
			array('image', 'file', 'types' => 'jpg, jpeg, gif, png', 'allowEmpty' => true),
			array('deliveryDate, notes', 'safe'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'unit' => 'Onde deseja retirar seu pedido?',
			'name' => 'Nome',
			'phone' => 'Telefone',
			'email' => 'E-mail',
			'quantity' => 'Quantidade / Peso',
			'deliveryDate' => 'Data / Hora Retirada',
			'image' => 'Imagem',
			'notes' => 'Observações',
		);
	}

	public function getUnitOptions()
	{
		return CHtml::listData(Unit::model()->findAll(array('order' => 'city, name')), 'id', 'name', 'city');
	}

	public function process()
	{
		$unit = Unit::model()->findByPk($this->unit);

		$message = new YiiMailMessage;
		$message->setTo($unit->email);
		$message->setFrom(Yii::app()->params['mail.senderEmail']);
		$message->setReplyTo($this->email);
		$body = Yii::app()->controller->renderPartial(
			'application.views.mail.internal.order', 
			array('orderForm' => $this, 'message' => $message), true
		);	
		$message->setBody(array('message' => $message, 'content' => $body), 'text/html');
		
		if ($this->image && $this->image instanceof CUploadedFile) {
			$path = APP_FILE_PATH . DIRECTORY_SEPARATOR . 'imagem.' . $this->image->extensionName;
			$this->image->saveAs($path);
			$message->attach(Swift_Attachment::fromPath($path));	
		}

		$send = Yii::app()->mail->send($message);
		return $send;
	}
}