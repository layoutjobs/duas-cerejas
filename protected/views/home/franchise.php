<?php 
$this->pageTitle = 'Seja um franqueado';
$this->pageSubtitle = 'Conheça as vantagens de ser um franqueado Duas Cerejas.'; 
$this->width = 10; 
$this->banners = array(4);
?>

<div class="panel">
	<div class="panel-body">
		<h2>Sobre a franquia</h2>
		<div class="row">
			<div class="col-md-6">
				<p>O sistema de franquias é um modelo de negócio no qual pessoas físicas ou jurídicas ingressam na rede de franquias desenvolvida por uma Franqueadora, passando a utilizar a sua marca e oferecer produtos e serviços na mesma forma negocial desenvolvida pela loja piloto.</p>
				<p>A Franquia é uma modalidade de parceria empresarial que se inicia no primeiro contato entre as partes, manifestado pela intenção do Candidato à Franquia.</p>
				<p>A adoção desse sistema desobriga o Franqueado a criar o seu próprio produto ou serviço e enfrentar o mercado de forma isolada, uma vez que conta com todo o apoio, know-how da marca já desenvolvida e testada pela Franqueadora.</p>
				<p>Assim, por definição, o sistema de franquias não configura grupo econômico, sociedade ou qualquer espécie de associação entre Franqueadora e Franqueados. Nele, cada Franqueado terá o seu próprio negócio, de forma independente da Franqueadora, não havendo, entre ambos, qualquer vínculo de ordem empregatícia nem tampouco consumerista.</p>
				<p>O Sistema de Franquias <strong>Duas Cerejas</strong> é aquele pelo qual a Franqueadora cederá o direito de atuar no setor de Alimentação, com a cessão do direito de uso da marca em locais previamente definidos, passando o Franqueado a contar com a ampla experiência desenvolvida pela Franqueadora nesse mercado.</p>
				<p>O Negócio Franqueado consiste na operação e gestão de uma loja especializada na fabricação e distribuição de produtos alimentícios (bolos e doces) e quaisquer outros produtos e serviços desenvolvido pela Franqueadora para venda na rede.</p>
			</div>
			<div class="col-md-6">
				<p>O exercício da prática já demonstrou com bastante clareza e convicção que só atende bem quem conhece o que faz, bem como os produtos e serviços que oferece. Isso sinaliza para o princípio de que só teremos um padrão de atendimento dentro dos critérios de excelência à medida que especializarmos nossas franquias e a quem nelas atuam.</p>
				<p>É preciso que desde o início o Franqueado saiba que uma franquia é um sistema aberto, cujos elementos estão dinamicamente relacionados entre si, que mantêm permanente interação com o ambiente, ou seja, com seus clientes, Franqueadora, fornecedoras, concorrentes, instituições governamentais, bancos e tantos outros.</p>
				<p>O <strong>capital inicial</strong> estimado para abertura de uma loja está em torno de R$ 210.000,00 já incluindo a taxa de franquia. A taxa de <strong>royalties</strong> é de 5% + 1% de taxa de propaganda com período de carência de seis meses.</p>
				<p>O <strong>ponto de equilíbrio</strong> é esperado em 8 a 12 meses e o <strong>retorno do investimento</strong> entre 24 e 36 meses.</p>
				<p>Valores e prazos podem variar dependendo da região e potencial do mercado.</p>
				<p>A <strong>Duas Cerejas</strong> está em plena expansão de suas lojas e convidamos você interessado em ter o seu próprio negócio, a conhecer as nossas lojas e obter mais informações de como fazer parte na nossa família.</p>
				<p>Utilize os formulários abaixo para obter mais informções, tirar dúvidas ou cadastrar uma franquia. Estamos aguardando o seu contato.<p>
			</div>
		</div>
	</div>
</div>

<div class="panel">
	<div class="panel-body">
		<h2>Cadastro de franquia</h2>

		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'franchiseApplicationForm',
			'enableAjaxValidation' => true,
			'enableClientValidation' => true,
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'errorCssClass' => 'has-error',
				'successCssClass' => 'has-success',
			),
		)); ?>

		<fieldset>
			<legend>Dados Pessoais</legend>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('name') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'name'); ?>
						<?php echo $form->textField($franchiseApplicationForm, 'name', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseApplicationForm, 'name', array('class' => 'help-block')); ?>
					</div>				
				</div>
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('email') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'email'); ?>
						<?php echo $form->textField($franchiseApplicationForm, 'email', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseApplicationForm, 'email', array('class' => 'help-block')); ?>
					</div>					
				</div>
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('birthDate') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'birthDate'); ?>
						<?php echo $form->textField($franchiseApplicationForm, 'birthDate', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseApplicationForm, 'birthDate', array('class' => 'help-block')); ?>
					</div>							
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('businessPhone') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'businessPhone'); ?>
						<?php echo $form->textField($franchiseApplicationForm, 'businessPhone', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseApplicationForm, 'businessPhone', array('class' => 'help-block')); ?>
					</div>				
				</div>
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('homePhone') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'homePhone'); ?>
						<?php echo $form->textField($franchiseApplicationForm, 'homePhone', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseApplicationForm, 'homePhone', array('class' => 'help-block')); ?>
					</div>				
				</div>
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('mobilePhone') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'mobilePhone'); ?>
						<?php echo $form->textField($franchiseApplicationForm, 'mobilePhone', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseApplicationForm, 'mobilePhone', array('class' => 'help-block')); ?>
					</div>						
				</div>
			</div>
		</fieldset>

		<fieldset>
			<legend>Endereço</legend>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('zip') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'zip'); ?>
						<?php echo $form->textField($franchiseApplicationForm, 'zip', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseApplicationForm, 'zip', array('class' => 'help-block')); ?>
					</div>				
				</div>
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('address') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'address'); ?>
						<?php echo $form->textField($franchiseApplicationForm, 'address', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseApplicationForm, 'address', array('class' => 'help-block')); ?>
					</div>				
				</div>
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('complement') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'complement'); ?>
						<?php echo $form->textField($franchiseApplicationForm, 'complement', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseApplicationForm, 'complement', array('class' => 'help-block')); ?>
					</div>						
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('neighborhood') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'neighborhood'); ?>
						<?php echo $form->textField($franchiseApplicationForm, 'neighborhood', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseApplicationForm, 'neighborhood', array('class' => 'help-block')); ?>
					</div>					
				</div>
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('city') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'city'); ?>
						<?php echo $form->textField($franchiseApplicationForm, 'city', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseApplicationForm, 'city', array('class' => 'help-block')); ?>
					</div>				
				</div>
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('state') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'state'); ?>
						<?php echo $form->textField($franchiseApplicationForm, 'state', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseApplicationForm, 'state', array('class' => 'help-block')); ?>
					</div>						
				</div>
			</div>
		</fieldset>

		<fieldset>
			<legend>Informações Comerciais</legend>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('equityCapital') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'equityCapital'); ?>
						<?php // echo $form->dropDownList($franchiseApplicationForm, 'equityCapital', $franchiseApplicationForm->equityCapitalOptions, array('class' => 'form-control', 'empty' => 'Selecione...')); ?>
						<?php echo $form->textField($franchiseApplicationForm, 'equityCapital', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseApplicationForm, 'equityCapital', array('class' => 'help-block')); ?>
					</div>					
				</div>
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('region') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'region'); ?>
						<?php // echo $form->dropDownList($franchiseApplicationForm, 'region', $franchiseApplicationForm->regionOptions, array('class' => 'form-control', 'empty' => 'Selecione...')); ?>
						<?php echo $form->textField($franchiseApplicationForm, 'region', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseApplicationForm, 'region', array('class' => 'help-block')); ?>
					</div>	
				</div>
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('businessArea') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'businessArea'); ?>
						<?php echo $form->textField($franchiseApplicationForm, 'businessArea', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseApplicationForm, 'businessArea', array('class' => 'help-block')); ?>
					</div>					
				</div>
			</div>
			<div class="row">
				<div class="col-sm-8">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('aditionalInformation') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'aditionalInformation'); ?>
						<?php echo $form->textArea($franchiseApplicationForm, 'aditionalInformation', array('class' => 'form-control', 'rows' => 4)); ?>
						<?php echo $form->error($franchiseApplicationForm, 'aditionalInformation', array('class' => 'help-block')); ?>
					</div>					
				</div>
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseApplicationForm->hasErrors('whereYouFindUs') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseApplicationForm, 'whereYouFindUs'); ?>
						<?php echo $form->textField($franchiseApplicationForm, 'whereYouFindUs', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseApplicationForm, 'whereYouFindUs', array('class' => 'help-block')); ?>
					</div>
					<button type="submit" class="btn btn-block btn-primary"><i class="fa fa-check"></i> Enviar</button>
				</div>
			</div>
		</fieldset>

		<?php $this->endWidget(); ?>
	</div>
</div>


<div class="panel panel-default">
	<div class="panel-body">
		<h2>Dúvidas</h2>

		<?php $form = $this->beginWidget('CActiveForm', array(
			'id' => 'franchiseContactForm',
			'enableAjaxValidation' => true,
			'enableClientValidation' => true,
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'errorCssClass' => 'has-error',
				'successCssClass' => 'has-success',
			),
		)); ?>

			<div class="row">
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseContactForm->hasErrors('name') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseContactForm, 'name'); ?>
						<?php echo $form->textField($franchiseContactForm, 'name', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseContactForm, 'name', array('class' => 'help-block')); ?>
					</div>				
				</div>
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseContactForm->hasErrors('email') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseContactForm, 'email'); ?>
						<?php echo $form->textField($franchiseContactForm, 'email', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseContactForm, 'email', array('class' => 'help-block')); ?>
					</div>					
				</div>
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseContactForm->hasErrors('birthDate') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseContactForm, 'birthDate'); ?>
						<?php echo $form->textField($franchiseContactForm, 'birthDate', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseContactForm, 'birthDate', array('class' => 'help-block')); ?>
					</div>							
				</div>
			</div>
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseContactForm->hasErrors('businessPhone') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseContactForm, 'businessPhone'); ?>
						<?php echo $form->textField($franchiseContactForm, 'businessPhone', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseContactForm, 'businessPhone', array('class' => 'help-block')); ?>
					</div>				
				</div>
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseContactForm->hasErrors('mobilePhone') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseContactForm, 'mobilePhone'); ?>
						<?php echo $form->textField($franchiseContactForm, 'mobilePhone', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseContactForm, 'mobilePhone', array('class' => 'help-block')); ?>
					</div>						
				</div>
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseContactForm->hasErrors('region') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseContactForm, 'region'); ?>
						<?php echo $form->textField($franchiseContactForm, 'region', array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseContactForm, 'region', array('class' => 'help-block')); ?>
					</div>				
				</div>
			</div>
			<div class="row">
				<div class="col-sm-8">
					<div class="form-group<?php echo $franchiseContactForm->hasErrors('message') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseContactForm, 'message'); ?>
						<?php echo $form->textArea($franchiseContactForm, 'message', array('class' => 'form-control', 'rows' => 4)); ?>
						<?php echo $form->error($franchiseContactForm, 'message', array('class' => 'help-block')); ?>
					</div>					
				</div>
				<div class="col-sm-4">
					<div class="form-group<?php echo $franchiseContactForm->hasErrors('reason') ? ' has-error' : ''; ?>">
						<?php echo $form->labelEx($franchiseContactForm, 'reason'); ?>
						<?php echo $form->dropDownList($franchiseContactForm, 'reason', $franchiseContactForm->reasonOptions, array('class' => 'form-control')); ?>
						<?php echo $form->error($franchiseContactForm, 'reason', array('class' => 'help-block')); ?>
					</div>
					<button type="submit" class="btn btn-block btn-primary"><i class="fa fa-check"></i> Enviar</button>
				</div>
			</div>

		<?php $this->endWidget(); ?>
	</div>
</div>