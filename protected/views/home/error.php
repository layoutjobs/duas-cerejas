<?php 
$this->width = 8;
$this->pageTitle = 'Erro ' . $code; 
?>

<p class="alert alert-error text-center"><?php echo CHtml::encode($message); ?></p> 