<?php 
$this->pageTitle = 'Bem-vindo';
$this->hideHeader = true; 
$productCategories = Category::model()->of('products')->hasParent()->findAll(array('order' => 't.sequence', 'limit' => 5));
?>

<div class="product-category-thumbnail-group">
	<div class="product-category-thumbnail-group-inner">
		<?php foreach ($productCategories as $category) : ?>

		<a class="product-category-thumbnail" href="<?php echo $this->createUrl('product/index', array('category' => $category->alias)); ?>" title="<?php echo CHtml::encode($category->name); ?>">
			<div class="product-category-thumbnail-inner">
				<?php if ($category->image) : ?>
				<img class="product-category-thumbnail-img" src="<?php echo $category->image->url; ?>" alt="<?php echo CHtml::encode($category->name); ?>">		
				<?php endif; ?>
				<div class="product-category-thumbnail-caption" style="background-image: url(<?php echo APP_IMG_URL . '/product-category-' . $category->alias; ?>-bg.jpg)">
					<h3 class="product-category-thumbnail-name"><?php echo CHtml::encode($category->name); ?></h3>
					<p class="product-category-thumbnail-description"><?php echo CHtml::encode($category->description); ?></p>
				</div>
			</div>
		</a>

		<?php endforeach; ?>
	</div>
</div>		