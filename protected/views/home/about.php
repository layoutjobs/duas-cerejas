<?php 
$this->pageTitle = 'Empresa';
$this->pageSubtitle = 'Conheça nossa doce história e descubra como este sonho se tornou realidade.'; 
$this->width = 8; 
$this->banners = array(1);
?>

<div class="panel">
	<div class="panel-body">
		<p><img class="img-responsive" src="<?php echo APP_IMG_URL; ?>/mkt-02.jpg"></p>		
		<h2>O sabor de um sonho realizado</h2>
		<p>A <strong>Duas Cerejas</strong> iniciou suas atividades em 27 de setembro de 2008, na cidade de Indaiatuba/SP. O surgimento da primeira loja, a qual recebeu o primeiro nome "A Casa do Bolo", foi através do sonho da Sra. Solange Maria ser dona de seu próprio negócio e ao mesmo tempo exercer o dom da confeitaria, unindo-se o necessário ao agradável. Assim, a empresa familiar no ramo de bolos e doces que visava na ocasião apenas atender as necessidades e paladares locais, através da gastronomia de bolos com um custo acessível a todas as classes sociais, foi sendo cada vez mais requisitada e indicada por aqueles que conheciam os produtos. Desta forma, seu crescimento foi mais rápido do que o esperado, mantendo-se firme no gosto dos consumidores até os dias atuais.<p>
		<p>A marca sempre buscou inovar nesse ramo gastronômico, através das deliciosas receitas criadas pela proprietária e confeiteira responsável, a Sra. Maria Solange M. Santos, deixando de lado aquele comércio engessado que só disponibiliza um tipo de produto; muito pelo contrário, à medida que os produtos eram procurados, imediatamente foram criadas novas receitas e automaticamente incluídas ao mix de produtos.<p>
		<p>A procura pelos produtos aumentou a cada dia, pessoas de diversos bairros e municípios vizinhos começaram a encomendar os bolos e doces, os quais sempre priorizaram a fabricação dos produtos com a utilização de ingredientes de boa qualidade, sendo um dos diferenciais em relação a outras marcas, a utilização das raspas de chocolates na maioria dos bolos.</p>
		<p>A <strong>Duas Cerejas</strong> atualmente possui duas unidades em Indaiatuba, uma em Itu e uma em Campinas, e trabalha com receitas próprias da marca, sendo bolos recheados, bolos caseiros, bolos de pasta americana, docinhos, bolos artísticos e de sorvete, na tentativa de alcançar todos os gostos e necessidades.</p>
		<p>Deste modo, para que o sonho se tornasse realidade, foram necessários alguns anos de trabalhos, realização de vários cursos de aprimoramento, com o intuito de obter um produto de qualidade para expansão do negócio e manter a produção dos mais saborosos bolos da região.</p>
	</div>
</div>
