<?php 
$message->subject = 'Envio de Publicação ' . CHtml::encode($postSubmition->title); 
$message->view = 'internal'; 
?>

<p>Olá.</p>

<p>Uma nova publicação <b><?php echo CHtml::encode($postSubmition->title) ; ?></b> foi enviada pelo site. Por favor, acesse o administrativo do site para aprovar ou reprovar a postagem.</p>

<p>Obrigado!</p>