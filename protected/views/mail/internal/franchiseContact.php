<?php 
$m = $franchiseContactForm;
$message->subject = 'Dúvida Sobre Franquia - ' . CHtml::encode($m->name); 
$message->view = 'internal'; 
?>

<p>Olá.</p>

<p><b><?php echo CHtml::encode($m->name); ?></b> tem uma dúvida sobre franquia.</p>

<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
	<tbody>

		<?php foreach ($m->attributes as $attributeName => $value) : ?>

		<?php
		if ($value && method_exists($m, ('get' . ucfirst($attributeName) . 'Options'))) 
			$value = $m->{$attributeName . 'Options'}[$value];
		?>

		<tr>
			<th align="right"><?php echo CHtml::encode($m->getAttributeLabel($attributeName)); ?></th>
			<td width="5%"></td>
			<td><?php echo CHtml::encode($value); ?></td>
		</tr>

		<?php endforeach; ?>

	</tbody>
</table>