<?php 
$message->subject = 'Pedido ' . CHtml::encode($orderForm->name); 
$message->view = 'internal'; 
?>

<p>Olá.</p>

<p><b><?php echo CHtml::encode($orderForm->name); ?></b> fez um novo pedido pelo site.</p>

<p>Ele(a) deseja <b><?php echo CHtml::encode($orderForm->quantity); ?></b> x <b><?php echo CHtml::encode($orderForm->product->name); ?></b> para <b><?php echo CHtml::encode($orderForm->deliveryDate); ?></b>.

<p><b>Observações:</b> <?php echo CHtml::encode($orderForm->notes); ?></p>

<p>Dúvidas, fale com ele(a) pelo telefone <b><?php echo CHtml::encode($orderForm->phone); ?></b>.</p>