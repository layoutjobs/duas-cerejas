<?php 
if (Yii::app()->request->getParam('modal'))
	$this->layout = 'modal';

$this->banners = array(5);
$this->width = 8;
$this->icon = 'map-marker';
$this->pageTitle = CHtml::encode($unit->city . ' - ' . $unit->name);
?>

<div class="unit-single">
	<?php if ($unit->image) : ?>

	<div class="unit-img">
		<img class="img-responsive" src="<?php echo $unit->image->url; ?>">
	</div>

	<?php endif; ?>

	<h2>Sobre</h2>
	<p class="unit-about"><?php echo $unit->about; ?></p>
	<address>
		<span class="unit-phone h2"><?php echo CHtml::encode($unit->phone); ?></span><br>
		<a class="unit-email" href="mailto:<?php echo CHtml::encode($unit->email); ?>"><?php echo CHtml::encode($unit->email); ?></a><br>
		<span class="unit-address"><?php echo CHtml::encode($unit->address . ' - ' .  $unit->city . '/' . $unit->state); ?></span>
	</address>
</div>

<?php if ($this->layout == 'modal') : ?>
	<img class="logo-icon" src="<?php echo APP_IMG_URL; ?>/logo-icon.png">
<?php endif; ?>