<?php 
$this->pageTitle = 'Unidades';
$this->pageSubtitle = 'Clique na unidade desejada para ver informaçãoes complementares.'; 
$this->banners = array(5);
?>

<div class="unit-group">
	<?php foreach ($units as $unit) : ?>

	<div class="unit row" data-target="#modal" data-toggle="modal" data-remote="<?php echo $this->createUrl('view', array('id' => $unit->id, 'modal' => true)); ?>">
		<div class="col col-3 col-xs-5 col-md-3">
			<h2 class="unit-name"><?php echo CHtml::encode($unit->city); ?><small><?php echo CHtml::encode($unit->name); ?></small></h2>
		</div>
		<div class="col col-3 col-md-3 visible-md visible-lg">
			<?php if (($thumbnail = $unit->getImage('thumbnail')) !== null) : ?>

			<div class="unit-img">
				<img src="<?php echo $unit->getImage('thumbnail')->url; ?>">
			</div>

			<?php endif; ?>
		</div>
		<div class="col col-3 col-xs-7 col-md-6">
			<address>
				<p class="unit-phone"><i class="fa fa-phone"></i> <span class="h2"><?php echo CHtml::encode($unit->phone); ?></span></p>
				<p class="unit-email"><i class="fa fa-envelope"></i> <span class="h4"><a href="mailto:<?php echo CHtml::encode($unit->email); ?>"><?php echo CHtml::encode($unit->email); ?></a></span></p>
				<p class="unit-address"><i class="fa fa-map-marker"></i> <span class="h4"><?php echo CHtml::encode($unit->address); ?></span></p>
			</address>
		</div>
	</div>

	<?php endforeach; ?>
</div>