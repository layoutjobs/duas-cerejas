<?php $model = isset($model) ? $model : new PostSubmitionForm; ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'postSubmitionForm',
	'method' => 'post',
	'action' => array('/post/submit'),
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'errorCssClass' => 'has-error',
		'successCssClass' => 'has-success',
	),
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	),
)); ?>

<div class="form-group<?php echo $model->hasErrors('title') ? ' has-error' : ''; ?>">
	<?php echo $form->labelEx($model, 'title'); ?>
	<?php echo $form->textField($model, 'title', array('class' => 'form-control')); ?>
	<?php echo $form->error($model, 'title', array('class' => 'help-block')); ?>
</div>	

<div class="form-group<?php echo $model->hasErrors('content') ? ' has-error' : ''; ?>">
	<?php echo $form->labelEx($model, 'content'); ?>
	<?php echo $form->textArea($model, 'content', array('class' => 'form-control')); ?>
	<?php echo $form->error($model, 'content', array('class' => 'help-block')); ?>
</div>	

<div class="form-group<?php echo $model->hasErrors('sender_name') ? ' has-error' : ''; ?>">
	<?php echo $form->labelEx($model, 'sender_name'); ?>
	<?php echo $form->textField($model, 'sender_name', array('class' => 'form-control')); ?>
	<?php echo $form->error($model, 'sender_name', array('class' => 'help-block')); ?>
</div>	

<div class="form-group<?php echo $model->hasErrors('sender_email') ? ' has-error' : ''; ?>">
	<?php echo $form->labelEx($model, 'sender_email'); ?>
	<?php echo $form->textField($model, 'sender_email', array('class' => 'form-control')); ?>
	<?php echo $form->error($model, 'sender_email', array('class' => 'help-block')); ?>
</div>	

<div class="form-group<?php echo $model->hasErrors('sender_friends_emails') ? ' has-error' : ''; ?>">
	<?php echo $form->labelEx($model, 'sender_friends_emails'); ?>
	<?php echo $form->textField($model, 'sender_friends_emails', array('class' => 'form-control')); ?>
	<?php echo $form->error($model, 'sender_friends_emails', array('class' => 'help-block')); ?>
	<p class="help-block"><i class="fa fa-info-circle"></i> Informe uma lista de e-mails separados por vírgula. Após a aprovação, os seus amigos serão avisados por e-mail.</p>
</div>	

<div class="form-group">
	<button type="submit" class="btn btn-primary btn-block btn-sm"><i class="fa fa-ok"></i> Enviar</button>
</div>					

<?php $this->endWidget(); ?>





