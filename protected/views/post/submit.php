<?php 
$this->icon = 'edit';
$this->width = 8;
$this->pageTitle = 'Envie uma publicação'; 
$this->pageSubtitle = 'Preencha o formulário abaixo, se a sua publicação for aceita, enviaremos um e-mail para você e seus amigos.';
?>

<div class="panel">
	<div class="panel-body">
		<h3>Envio de fotos</h3>

		<?php $this->widget('application.widgets.upload.Upload', array(
			'id' => 'postSubmitionUpload',
			'url' => $this->createUrl('postSubmitionUpload'),
		)); ?>

		<p class="help-block"><i class="fa fa-info-circle"></i> Evite enviar images com nomes contendo 
		espaços, acentos ou caracteres especiais (Ç, !, ?, etc.), assim como nomes muito  longos 
		(máximo 128 caracteres). São aceitos arquivos com as seguintes extensões: 
		<strong>.gif, .jpg e .png</strong>.</p>

		<h3>Dados da publicação</h3>

		<?php $this->renderPartial('_postSubmitionForm', array('model' => $postSubmitionForm)); ?>
	</div>
</div>
