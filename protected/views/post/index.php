<?php 
$this->pageTitle = 'Blog';
$this->pageSubtitle = 'Fique por dentro das novidades Duas Cerejas.'; 
$this->banners = array(5);
?>

<div class="row">
	<div class="col-md-8">
		<div class="post-group">
			<?php foreach ($posts as $post) : ?>

			<div class="panel">
				<div class="panel-body">
					<article class="post">
						<h2 class="post-title"><?php echo CHtml::encode($post->title); ?></h2>
						<p class="post-meta">
							<?php $publishDate = new DateTime($post->parseDateTime('publish_date')); ?> 			
							<time class="post-publish-date" datetime="<?php echo $publishDate->format('Y-m-d'); ?>">
								<i class="fa fa-clock-o"></i>
								<?php echo $publishDate->format('d/m/Y'); ?>
							</time>
							<span class="post-author">
								<i class="fa fa-user"></i>
								<?php echo $post->author->name; ?>
							</span>
						</p>

						<?php if ($post->image) : ?>

						<figure class="post-img">
							<img src="<?php echo $post->image->url; ?>">
						</figure>
						
						<?php endif; ?>

						<?php if ($gallery = $post->getFiles('gallery', 'large')) : ?>

						<div class="post-gallery owl-carousel">
							<?php foreach ($gallery as $image) : ?>

							<a href="<?php echo $image->url; ?>" rel="prettyPhoto[<?php echo $post->id; ?>]" class="item"><img src="<?php echo $image->url; ?>"></a>

							<?php endforeach; ?>
						</div>

						<?php endif; ?>

						<p class="post-excerpt"><?php echo $post->excerpt; ?> <a href="<?php echo $this->createViewUrl($post); ?>">[ + ] Continue Lendo</a></p>
					</article>
				</div>
			</div>

			<?php endforeach; ?>

			<?php if ($pagination->pageCount > 1) : ?>

			<ul class="pager">

				<?php if ($pagination->currentPage < ($pagination->pageCount - 1))  : ?>

				<li class="previous"><a href="<?php echo $pagination->createPageUrl($this, $pagination->currentPage + 1); ?>"><i class="fa fa-chevron-left"></i> Mais Antigos</a></li>

				<?php endif; ?>

				<?php if ($pagination->currentPage > 0)  : ?>

				<li class="next"><a href="<?php echo $pagination->createPageUrl($this, ($pagination->currentPage - 1)); ?>">Mais Recentes <i class="fa fa-chevron-right"></i></a></li>

				<?php endif; ?>
			</ul>

			<?php endif; ?>
		</div>
	</div>
	<div class="col-md-4">
		<div class="panel">
			<div class="panel-body">
				<h3 class="panel-body-title">Categorias</h3>
				<ul class="nav nav-side">
					<?php foreach ($categories as $category) : ?>

					<li<?php echo $currentCategory && $category->id === $currentCategory->id ? ' class="active"' : ''; ?>>
						<a href="<?php echo $this->createViewUrl($category); ?>"><?php echo CHtml::encode($category->name); ?></a>
					</li>

					<?php endforeach; ?>
				</ul>
			</div>
		</div>

		<a class="btn btn-primary btn-block" href="<?php echo $this->createUrl('submit'); ?>">Envie uma Publicação</a>
	</div>
</div>

<?php 
$js = <<<JS
$('.post-gallery.owl-carousel').owlCarousel({
    loop: true,
    items: 2,
    margin: 5,
	autoplay: true,
    autoplayTimeout: 8000,
    autoplayHoverPause: true,
	responsive: {
		768: {
		    items: 3
		},
		992: {
		    items: 3
		},
		1200: {
		    items: 4
		}
	}
});

$("a[rel^='prettyPhoto']").prettyPhoto();
JS;
Yii::app()->clientScript->registerPackage('owl.carousel'); 
Yii::app()->clientScript->registerPackage('prettyPhoto'); 
Yii::app()->clientScript->registerScript('post.index', $js, CClientScript::POS_END); 
?>