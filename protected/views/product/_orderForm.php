<?php $model = isset($model) ? $model : new OrderForm; ?>

<?php $form = $this->beginWidget('CActiveForm', array(
	'id' => 'orderForm',
	'method' => 'post',
	'action' => array('/product/order', 'id' => $product->id),
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'clientOptions' => array(
		'validateOnSubmit' => true,
		'errorCssClass' => 'has-error',
		'successCssClass' => 'has-success',
	),
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	),
)); ?>


<div class="row">
	<div class="col-sm-6">
		<div class="form-group<?php echo $model->hasErrors('name') ? ' has-error' : ''; ?>">
			<?php echo $form->labelEx($model, 'name'); ?>
			<?php echo $form->textField($model, 'name', array('class' => 'form-control')); ?>
			<?php echo $form->error($model, 'name', array('class' => 'help-block')); ?>
		</div>			
	</div>
	<div class="col-sm-6">
		<div class="form-group<?php echo $model->hasErrors('unit') ? ' has-error' : ''; ?>">
			<?php echo $form->labelEx($model, 'unit'); ?>
			<?php echo $form->dropDownList($model, 'unit', $model->unitOptions, array('class' => 'form-control', 'empty' => '')); ?>
			<?php echo $form->error($model, 'unit', array('class' => 'help-block')); ?>
		</div>				
	</div>
</div>

<div class="row">
	<div class="col-sm-6">
		<div class="form-group<?php echo $model->hasErrors('phone') ? ' has-error' : ''; ?>">
			<?php echo $form->labelEx($model, 'phone'); ?>
			<?php echo $form->textField($model, 'phone', array('class' => 'form-control')); ?>
			<?php echo $form->error($model, 'phone', array('class' => 'help-block')); ?>
		</div>				
	</div>
	<div class="col-sm-6">
		<div class="form-group<?php echo $model->hasErrors('email') ? ' has-error' : ''; ?>">
			<?php echo $form->labelEx($model, 'email'); ?>
			<?php echo $form->textField($model, 'email', array('class' => 'form-control')); ?>
			<?php echo $form->error($model, 'email', array('class' => 'help-block')); ?>
		</div>					
	</div>
</div>
<div class="row">
	<div class="col-sm-6">
		<div class="form-group<?php echo $model->hasErrors('quantity') ? ' has-error' : ''; ?>">
			<?php echo $form->labelEx($model, 'quantity'); ?>
			<?php echo $form->textField($model, 'quantity', array('class' => 'form-control')); ?>
			<?php echo $form->error($model, 'quantity', array('class' => 'help-block')); ?>
		</div>					
	</div>
	<div class="col-sm-6">
		<div class="form-group<?php echo $model->hasErrors('deliveryDate') ? ' has-error' : ''; ?>">
			<?php echo $form->labelEx($model, 'deliveryDate'); ?>
			<?php echo $form->textField($model, 'deliveryDate', array('class' => 'form-control')); ?>
			<?php echo $form->error($model, 'deliveryDate', array('class' => 'help-block')); ?>
		</div>				
	</div>
</div>
<div class="form-group<?php echo $model->hasErrors('notes') ? ' has-error' : ''; ?>">
	<?php echo $form->labelEx($model, 'notes'); ?>
	<?php echo $form->textArea($model, 'notes', array('class' => 'form-control')); ?>
	<?php echo $form->error($model, 'notes', array('class' => 'help-block')); ?>
</div>
<div class="row">
	<div class="col-sm-6">
		<div class="form-group<?php echo $model->hasErrors('image') ? ' has-error' : ''; ?>">
			<?php echo $form->labelEx($model, 'image'); ?>
			<?php echo $form->fileField($model, 'image', array('class' => 'form-control')); ?>
			<?php echo $form->error($model, 'image', array('class' => 'help-block')); ?>
		</div>
	</div>
	<div class="col-sm-6">
		<div class="form-group">
			<label class="control-label">&nbsp;</label>
			<button type="submit" class="btn btn-primary btn-block btn-sm"><i class="fa fa-ok"></i> Enviar Pedido</button>
		</div>					
	</div>
</div>

<?php $this->endWidget(); ?>





