<?php 
if (Yii::app()->request->getParam('modal'))
	$this->layout = 'modal';

$this->icon = 'edit';
$this->width = 8;
$this->pageTitle = 'Pedido'; 
$this->pageSubtitle = 'Preencha os itens abaixo e em breve entraremos em contato.';
$this->banners = array(2);
?>

<div class="product-order">
	<div class="product">
		<div class="row">
			<div class="col-xs-6">
				<p><img class="img-responsive" src="<?php echo $product->image ? $product->image->url : null; ?>" alt="<?php echo CHtml::encode($product->name); ?>"></p>
			</div>
			<div class="col-xs-6">
				<h3><?php echo CHtml::encode($product->name); ?></h3>
				<p><?php echo CHtml::encode($product->description); ?></p>				
			</div>
		</div>
	</div>

	<?php $this->renderPartial('_orderForm', array('product' => $product, 'model' => $orderForm)); ?>
</div>






