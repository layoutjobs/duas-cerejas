<?php 
$this->pageTitle = 'Bolos';
$this->pageSubtitle = 'Navegue entre as saborosas opções de bolos, faça sua escolha e nos envie seu pedido.'; 
$this->banners = array(2);

$categoriesParents = array();
$categoriesByParent = array();
$categoriesWithProducts = array();

foreach ($categories as $category) {
	$categoriesParents[$category->parent_id] = $category->parent;
	$categoriesByParent[$category->parent_id][$category->id] = $category;

	if ($category->products)
		$categoriesWithProducts[] = $category;
}
?>

<div id="produtos" class="row">
	<div class="col-md-2">
		<nav class="product-category-sidenav" role="complementary">	
			<a class="dropdown-toggle" data-toggle="dropdown">
				<?php echo (null !== $currentCategory) ? CHtml::encode($currentCategory->name) : 'Categoria'; ?> 
				<span class="caret"></span>	
			</a>
			<ul class="dropdown-menu" role="menu">
				<?php foreach ($categoriesByParent as $parentId => $categories) : ?>

				<li class="dropdown-header"><?php echo CHtml::encode($categoriesParents[$parentId]->name); ?></li>

				<?php foreach ($categories as $category) : ?>

				<li<?php echo $category->id == $currentCategory->id ? ' class="active"' : ''; ?>>
					<a href="<?php echo $this->createUrl('index', array('category' => $category->alias)); ?>">
						<?php echo CHtml::encode($category->name); ?>
					</a>
				</li>

				<?php endforeach; ?>

				<?php endforeach; ?>
			</ul>
		
			<ul class="nav">
				<?php foreach ($categoriesByParent as $parentId => $categories) : ?>

				<li>
					<ul class="nav">
						<li><h4><?php echo CHtml::encode($categoriesParents[$parentId]->name); ?></h4></li>

						<?php foreach ($categories as $category) : ?>

						<li<?php echo $category->id == $currentCategory->id ? ' class="active"' : ''; ?>>
							<a href="<?php echo $this->createUrl('index', array('category' => $category->alias)); ?>#produtos">
								<?php echo CHtml::encode($category->name); ?>
							</a>
						</li>

						<?php endforeach; ?>
					</ul>						
				</li>

				<?php endforeach; ?>
			</ul>
		</nav>
	</div>
	<div class="col-md-8">
		<?php if ($products) : ?>

		<?php $this->renderPartial('_orderForm', array('product' => $products[0]), true); ?>

		<div class="product-group">
			<div id="productGroupCarousel" class="carousel slide" data-ride="carousel">
				<div class="carousel-inner">

					<?php foreach ($products as $i => $product) : ?>
				
					<div class="product item<?php echo $i === 0 ? ' active' : ''; ?>" data-view-url="<?php echo $this->createUrl('order', array('id' => $product->id, 'modal' => true)); ?>">
						<div class="block-fluid-group">
							<img class="product-img block-fluid block-fluid-middle-center" src="<?php echo $product->image ? $product->image->url : null; ?>" alt="<?php echo CHtml::encode($product->name); ?>">
						</div>
						<div class="carousel-caption">
							<h2 class="product-name"><?php echo CHtml::encode($product->name); ?></h2>
							<p class="product-description"><?php echo CHtml::encode($product->description); ?></p>								

							<?php if ($product->sweetness > 0) : ?>

							<p class="product-sweetness">
								<img class="product-sweetness-img" src="<?php echo APP_IMG_URL; ?>/product-sweetness-<?php echo CHtml::encode($product->sweetness); ?>.png">
								<span class="product-sweetness-label">Medidor de Doçura</span>
							</p>

							<?php endif; ?>								
						</div>
					</div>

					<?php endforeach; ?>
				</div>

				<?php if (count($products) > 1) : ?>

				<a class="left carousel-control" href="#productGroupCarousel" data-slide="prev"><span class="icon-prev"></span></a>
				<a class="right carousel-control" href="#productGroupCarousel" data-slide="next"><span class="icon-next"></span></a>
			
				<?php endif; ?>
			</div>
			<button class="btn btn-primary btn-lg product-order-btn">
				<span class="h4"><i class="fa fa-edit"></i> FAZER PEDIDO</span>
			</button>			
		</div>

		<?php else : ?>

		<div class="panel">
			<div class="panel-body">
				<?php 
				$categoriesLabel = '';
				foreach ($categoriesWithProducts as $category) {
					$categoriesLabel .= CHtml::link($category->name, $this->createUrl('index', array('category' => $category->alias)), array('class' => 'label label-primary'));
					$categoriesLabel .= ' ';
				}
				?>

				<h2>Em construção</h2>
				<p>Desculpe, mas não há produtos cadastros nesta categoria no momento. Por favor, retorne em breve.<br>
				Enquanto isso, que tal consultar alguma das categorias abaixo?</p>
				<p><?php echo $categoriesLabel; ?></p>
			</div>
		</div>

		<?php endif; ?>
	</div>

	<div class="col-md-2">
		<div class="product-thumbnail-group">
			<div id="productThumbnailGroupCarousel" class="carousel carousel-vertical slide" data-ride="carousel">
				<div class="carousel-inner">
					<?php $productsGroup = array_chunk($products, 4); ?>

					<?php foreach ($productsGroup as $i => $products) : ?>

					<div class="item<?php echo $i === 0 ? ' active' : ''; ?>">

					<?php foreach ($products as $ii => $product) : ?>					
					
					<div class="product-thumbnail" data-target="#productGroupCarousel" data-slide-to="<?php echo ($i * 4) + $ii; ?>">
						<div class="block-fluid-group">
							<img class="product-thumbnail-img block-fluid block-fluid-middle-center" alt="<?php echo CHtml::encode($product->name); ?>" src="<?php echo $product->image ? $product->image->url : null; ?>">								
						</div>
						<h5 class="product-thumbnail-title"><?php echo CHtml::encode($product->name); ?></h5>
					</div>

					<?php endforeach; ?>

					</div>

					<?php endforeach; ?>
				</div>
				<a class="left carousel-control" href="#productThumbnailGroupCarousel" data-slide="prev"><span class="fa fa-chevron-up"></span></a>
				<a class="right carousel-control" href="#productThumbnailGroupCarousel" data-slide="next"><span class="fa fa-chevron-down"></span></a>
			</div>
		</div>
	</div>
</div>

<?php 
$js = <<<JS
$('.product-order-btn').on('click', function (e) {
	e.preventDefault();
	var url = $(this).parents('.product-group').find('[data-view-url].active').attr('data-view-url');

	$('#modal').modal({ 'remote': url });
});
JS;
Yii::app()->clientScript->registerPackage('owl.carousel'); 
Yii::app()->clientScript->registerPackage('prettyPhoto'); 
Yii::app()->clientScript->registerScript('post.index', $js, CClientScript::POS_END); 
?>