<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">&times;</button>
	<h2 class="modal-title">
		<?php if ($this->icon) : ?>
			<i class="fa fa-<?php echo $this->icon; ?>"></i>
		<?php endif; ?>

		<?php echo CHtml::encode($this->pageTitle); ?>

		<?php if ($this->pageSubtitle) : ?>
			<small><?php echo CHtml::encode($this->pageSubtitle); ?></small>
		<?php endif; ?>
	</h2>
</div>
<div class="modal-body">
	<?php echo $content; ?>
</div>

<?php Yii::app()->clientScript->reset(); ?>