<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="pt-br" class="no-js no-swf lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="pt-br" class="no-js no-swf lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="pt-br" class="no-js no-swf lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="pt-br" class="no-js no-swf"> <!--<![endif]-->
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="description" content="<?php echo CHtml::encode(Yii::app()->params['site.description']); ?>">
		<meta name="keywords" content="<?php echo CHtml::encode(Yii::app()->params['site.keywords']); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<title><?php echo CHtml::encode(($this->pageTitle ? $this->pageTitle . ' - ' : null) . Yii::app()->name); ?></title>

		<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/bootstrap.css'); ?>
		<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/font-awesome.css'); ?>
		<?php Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl . '/css/main.css'); ?>	

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->

		<link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet">
		<link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:300,300italic,400,400italic,700,700italic" rel="stylesheet">
	</head>
	<body>
		<!--[if lt IE 9]>
		<p class="chromeframe" style="position: fixed; left: 0; right: 0; bottom: 0; z-index: 5; padding: 20px; background: #fff; text-align: center;">Você está usando um navegador <strong>desatualizado</strong>. Por favor, <a href="http://browsehappy.com/">atualize seu navegador</a> ou <a href="http://www.google.com/chromeframe/?redirect=true">ative o Google Chrome Frame</a> para melhorar sua experiência.</p>
		<![endif]-->



		<!-- HEADER
		================================================== -->

		<header class="main-header">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<a href="<?php echo Yii::app()->homeUrl; ?>" class="logo">
							<img class="logo-img" src="<?php echo APP_IMG_URL; ?>/logo.png" alt="Duas Cerejas">
						</a>
					</div>
					<div class="col-md-5 text-center">
						<a href="http://www.duascerejas.com.br/cardapio/cardapio-duascerejas.pdf" target="_blank" class="menu hidden-swf">
							<span class="fa fa-file-text-o"></span><small>Clique para acessar nosso</small>Cardápio Online
						</a>
						<a href="http://www.duascerejas.com.br/cardapio" target="_blank" class="menu hidden-no-swf">
							<span class="fa fa-file-text-o"></span><small>Clique para acessar nosso</small>Cardápio Online
						</a>
						<nav role="navigation">
							<ul class="nav">
								<li<?php echo $this->id === 'home' && $this->action->id === 'index' ? ' class="active"' : '' ?>>
									<a href="<?php echo Yii::app()->homeUrl; ?>"><span class="h4">Home</span></a>
								</li>
								<li<?php echo $this->id === 'product' ? ' class="active"' : '' ?>>
									<a href="<?php echo Yii::app()->createUrl('/product'); ?>"><span class="h4">Produtos</span></a>
								</li>
								<li<?php echo $this->id === 'home' && $this->action->id === 'about' ? ' class="active"' : '' ?>>
									<a href="<?php echo Yii::app()->createUrl('/home/about'); ?>"><span class="h4">Empresa</span></a>
								</li>						
								<li<?php echo $this->id === 'unit' ? ' class="active"' : '' ?>>
									<a href="<?php echo Yii::app()->createUrl('/unit'); ?>"><span class="h4">Lojas</span></a>
								</li>
								<li<?php echo $this->id === 'post' ? ' class="active"' : '' ?>>
									<a href="<?php echo Yii::app()->createUrl('/post'); ?>"><span class="h4">Blog</span></a>
								</li>
							</ul>
						</nav>
					</div>
					<div class="col-md-3">
						<div class="box-franquia">
							<span class="h4">Quer fazer sucesso conosco?</span>
							<a href="<?php echo Yii::app()->createUrl('/home/franchise'); ?>">
								<span class="h4"><i class="fa fa-arrow-circle-right"></i> Seja um franqueado</span>
								<img src="<?php echo APP_IMG_URL; ?>/mkt-01.png">
							</a>
						</div>
					</div>
				</div>
			</div>
		</header>



		<!-- BANNER A
		================================================== -->

		<?php $this->renderPartial('/layouts/_bannerA'); ?>



		<!-- ALERTS
		================================================== -->

		<div class="main-alerts">
			<?php if ($alert = Yii::app()->user->getAlert(WebUser::ALERT_SUCCESS)) : ?>
				<div class="alert alert-success alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
					<strong>Sucesso!</strong> <?php echo $alert; ?>
				</div>
			<?php endif; ?>

			<?php if ($alert = Yii::app()->user->getAlert(WebUser::ALERT_INFO)) : ?>
				<div class="alert alert-info alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
					<strong>Info!</strong> <?php echo $alert; ?>
				</div>
			<?php endif; ?>

			<?php if ($alert = Yii::app()->user->getAlert(WebUser::ALERT_WARNING)) : ?>
				<div class="alert alert-warning alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
					<strong>Atencão!</strong> <?php echo $alert; ?>
				</div>
			<?php endif; ?>

			<?php if ($alert = Yii::app()->user->getAlert(WebUser::ALERT_DANGER)) : ?>
				<div class="alert alert-danger alert-dismissible" role="alert">
					<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Fechar</span></button>
					<strong>Ops. Algo deu errado.</strong> <?php echo $alert; ?>
				</div>
			<?php endif; ?>
		</div>



		<!-- CONTENT
		================================================== -->

		<?php if ($this->width) : ?>

		<section class="main-section">
			<?php if (!$this->hideHeader) : ?>

			<div class="main-section-header">
				<div class="container">
					<?php if ($this->width < 12) : ?>

					<div class="row">
						<div class="col-md-<?php echo $this->width . ' col-md-offset-' . ((12 - $this->width) / 2); ?>">
							<h1 class="main-section-title"><?php echo $this->pageTitle; ?></h1>
							<?php if ($this->pageSubtitle) : ?>
							<p class="main-section-lead"><?php echo $this->pageSubtitle; ?></p>
							<?php endif; ?>
						</div>
					</div>

					<?php else : ?>

					<h1 class="main-section-title"><?php echo $this->pageTitle; ?></h1>
					<?php if ($this->pageSubtitle) : ?>
					<p class="main-section-lead"><?php echo $this->pageSubtitle; ?></p>
					<?php endif; ?>

					<?php endif; ?>
				</div>
			</div>

			<?php endif; ?>

			<div class="container">
				<?php if ($this->width < 12) : ?>

				<div class="row">
					<div class="col-md-<?php echo $this->width . ' col-md-offset-' . ((12 - $this->width) / 2); ?>">
						<?php echo $content; ?>
					</div>
				</div>

				<?php else : ?>

				<?php echo $content; ?>
				<?php $this->renderPartial('/layouts/_bannerB'); ?>

				<?php endif; ?>
			</div>
		</section>

		<?php else : ?>

		<?php echo $content; ?>
		<?php $this->renderPartial('/layouts/_bannerB'); ?>

		<?php endif; ?>



		<!-- FOOTER
		================================================== -->

		<footer class="main-footer">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						<img src="<?php echo APP_IMG_URL; ?>/logo-icon.jpg">

						<?php if (Yii::app()->params['facebook.url']) : ?>

						<a class="btn-facebook" target="_blank" href="<?php echo Yii::app()->params['facebook.url']; ?>"><i class="fa fa-facebook-square"></i></a>

						<?php endif; ?>
					</div>
					<div class="col-md-3">
						<h4>Acesso Restrito</h4>
						<div class="login">
							<form class="login-form" action="<?php echo Yii::app()->baseUrl; ?>/admin/home/login" method="post" role="form">
								<div class="form-group">
									<input class="form-control input-sm" type="text" name="LoginForm[username]" placeholder="Nome de Usuário">
								</div>
								<div class="form-group">
									<div class="input-group">
										<input class="form-control input-sm" type="password" name="LoginForm[password]" placeholder="Senha">
										<span class="input-group-btn">
											<button type="submit" class="btn btn-primary btn-sm">OK</button>
										</span>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-md-2">				
						<h4>Seja um franqueado</h4>
						<nav role="navigation">
							<ul>
								<li><a href="<?php echo Yii::app()->createUrl('/home/franchise'); ?>">Franquia</a></li>
							</ul>
						</nav>
					</div>
					<div class="col-md-2">
						<h4>Mapa do site</h4>
						<nav role="navigation">
							<ul>
								<li><a href="<?php echo Yii::app()->homeUrl; ?>">Home</a></li>
								<li><a href="<?php echo Yii::app()->createUrl('/home/franchise'); ?>">Franquia</a></li>
								<li><a href="<?php echo Yii::app()->createUrl('/product'); ?>">Produtos</a></li>
								<li><a href="<?php echo Yii::app()->createUrl('/home/about'); ?>">Empresa</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</footer>



		<!-- MODAL
		================================================== -->

		<div class="modal fade" id="modal" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content"></div>
			</div>
		</div>



		<!-- LOADING
		================================================== -->

		<div id="ajax-loading"></div>



		<!-- SCRIPTS
		================================================== -->

		<?php if (Yii::app()->params['ga.trackingId']) : ?>

		<script>
		var _gaq=[['_setAccount','<?php echo Yii::app()->params['ga.trackingId'] ?>'],['_trackPageview']];
		(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
		g.src='//www.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g,s)}(document,'script'));
		</script>

		<?php endif; ?>

		<?php if (isset($_GET['r']) && strpos($_GET['r'], 'acasadoboloonline') !== false) : ?>

		<div id="popup" class="modal fade">
		  <div class="modal-dialog">
		    <div class="modal-content">
					<img class="img-responsive" src="<?php echo APP_IMG_URL; ?>/popup.jpg">
		    </div>
		  </div>
		</div>

		<style>
			#popup .modal-content {	background: #D2AB68; padding: 10px; }
			@media (min-width: 800px) { #popup .modal-dialog { width: 800px; } }
		</style>

		<script>
		$(document).ready(function() {
			$('#popup').modal('show');
			$('#popup').on('click', function() { $(this).modal('hide') });
		});
		</script>

		<?php endif; ?>

		<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
		<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap.js', CClientScript::POS_END); ?>
		<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/common.js', CClientScript::POS_END); ?>
		<?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/main.js', CClientScript::POS_END); ?>
	</body>
</html>