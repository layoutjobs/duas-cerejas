<?php if (($banners = Banner::model()->position('banner-a')->route($this->route)->active()->asc()->findAll()) !== null) : ?>

<section class="banner-a">
	<div id="bannerACarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<?php foreach ($banners as $i => $banner) : ?>

			<div class="item<?php echo $i === 0 ? ' active' : ''; ?>" style="background-image: url(<?php echo $banner->image ? $banner->image->url : null; ?>);">
				<?php if ($banner->url) : ?>
					<a href="<?php echo $banner->url; ?>"></a>
				<?php endif; ?>

				<div class="carousel-caption">
					<div class="container">														
						<h2><i class="fa fa-arrow-circle-right"></i> <?php echo CHtml::encode($banner->name); ?></h2>
					</div>
				</div>
			</div>	

			<?php endforeach; ?>
		</div>

		<?php if (count($banners) > 1) : ?>

		<div class="carousel-controls">
			<div class="container">
				<a class="left carousel-control" href="#bannerACarousel" data-slide="prev"><span class="icon-prev"></span></a>
				<a class="right carousel-control" href="#bannerACarousel" data-slide="next"><span class="icon-next"></span></a>
			</div>
		</div>

		<?php endif; ?>
	</div>
</section>

<?php endif; ?>