<?php if (($banners = Banner::model()->position('banner-b')->route($this->route)->active()->asc()->findAll()) !== null) : ?>

<section class="banner-b">
	<div id="bannerBCarousel" class="carousel slide" data-ride="carousel">
		<div class="carousel-inner">
			<?php foreach ($banners as $i => $banner) : ?>

			<div class="item<?php echo $i === 0 ? ' active' : ''; ?>">
				<?php if ($banner->url) : ?>
				<a href="<?php echo $banner->url; ?>">
					<img src="<?php echo $banner->image ? $banner->image->url : null; ?>" alt="<?php echo CHtml::encode($banner->name); ?>">
				</a>
				<?php else : ?>
				<img src="<?php echo $banner->image ? $banner->image->url : null; ?>" alt="<?php echo CHtml::encode($banner->name); ?>">
				<?php endif; ?>
			</div>	

			<?php endforeach; ?>
		</div>

		<?php if (count($banners) > 1) : ?>

		<div class="carousel-controls">
			<div class="container">
				<a class="left carousel-control" href="#bannerBCarousel" data-slide="prev"><span class="icon-prev"></span></a>
				<a class="right carousel-control" href="#bannerBCarousel" data-slide="next"><span class="icon-next"></span></a>
			</div>
		</div>

		<?php endif; ?>
	</div>
</section>

<?php endif; ?>