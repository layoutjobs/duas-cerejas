<?php

class ProductController extends Controller
{
	public function actionIndex($category = null, $id = null) 
	{
		$categories = Category::model()
			->of('products')
			->isChild()
			->findAll(array(
				'index' => 'id',
				'order' => 't.name',
			));

		if ($category === null) {
			$currentCategory = reset($categories);
			$this->redirect(array('product/index', 'category' => $currentCategory->alias));
		} else {
			foreach ($categories as $item) {
				if ($item->alias == $category) {
					$currentCategory = $item;
					break;
				}
			}

			if (empty($currentCategory)) {
				$currentCategory = Category::model()->findByAttributes(array('alias' => $category));
				$products = Product::model()->with('categories.parent')->findAll(array(
					'condition' => 'parent.id = :category',
					'params' => array(':category' => $currentCategory->id),
				));

				if ($currentCategory === null)
					throw new CHttpException(404, 'Página não encontrada.');
			}
		}

		if (empty($products))
			$products = $currentCategory->products(array('order' => 'name'));

		$this->render('index', array(
			'currentCategory' => $currentCategory,
			'categories' => $categories, 
			'products' => $products, 
		));
	}

	public function actionOrder($id)
	{
		$product = Product::model()->findByPk($id);

		if ($product === null)
			throw new CHttpException(404, 'Página não encontrada.');

		$orderForm = new OrderForm;
		$orderForm->product = $product;
		
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'orderForm') {
			echo CActiveForm::validate($orderForm);
			Yii::app()->end();
		}
		
		if (isset($_POST['OrderForm'])) {
			$orderForm->attributes = $_POST['OrderForm'];
			$orderForm->image = CUploadedFile::getInstance($orderForm, 'image');

			if ($orderForm->validate()) {
				if ($orderForm->process())
					Yii::app()->user->setAlert(WebUser::ALERT_SUCCESS, 'Seu pedido foi enviado com sucesso.');					
				else
					Yii::app()->user->setAlert(WebUser::ALERT_DANGER, 'Não foi possível enviar o pedido. Por favor, tente novamente mais tarde.');

				$this->redirect(Yii::app()->homeUrl);
			} else
				Yii::app()->user->setAlert(WebUser::ALERT_DANGER, current(current($orderForm->getErrors())));
		}

		$this->render('order', array('orderForm' => $orderForm, 'product' => $product));
	}
}

