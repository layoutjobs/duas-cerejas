<?php

class UnitController extends Controller
{
	public function actionIndex() 
	{
		$units = Unit::model()->findAll();

		$this->render('index', array('units' => $units));
	}

	public function actionView($id)
	{	
		$unit = Unit::model()->findByPk($id);

		if ($unit === null)
			throw new CHttpException(404, 'Página não encontrada.');

		$this->render('view', array('unit' => $unit));
	}
}

