<?php

class HomeController extends Controller
{
	public function actionIndex() 
	{
		$this->render('index');
	}

	public function actionAbout() 
	{
		$this->render('about');
	}

	public function actionFranchise() 
	{
		$franchiseApplicationForm = new FranchiseApplicationForm;
		
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'franchiseApplicationForm') {
			echo CActiveForm::validate($franchiseApplicationForm);
			Yii::app()->end();
		}
		
		if (isset($_POST['FranchiseApplicationForm'])) {
			$franchiseApplicationForm->attributes = $_POST['FranchiseApplicationForm'];

			if ($franchiseApplicationForm->validate() && $franchiseApplicationForm->process()) {
				Yii::app()->user->setAlert(WebUser::ALERT_SUCCESS, 'Seu solicitação foi enviado com sucesso. Por favor, aguarde nosso contato.');

				if (isset($_POST['returnUrl']))
					$this->redirect($_POST['returnUrl']);
				else
					$this->redirect(array('index'));
			} else
				Yii::app()->user->setAlert(WebUser::ALERT_DANGER, current(current($franchiseApplicationForm->getErrors())));
		}

		$franchiseContactForm = new FranchiseContactForm;
		
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'franchiseContactForm') {
			echo CActiveForm::validate($franchiseContactForm);
			Yii::app()->end();
		}
		
		if (isset($_POST['FranchiseContactForm'])) {
			$franchiseContactForm->attributes = $_POST['FranchiseContactForm'];

			if ($franchiseContactForm->validate() && $franchiseContactForm->process()) {
				Yii::app()->user->setAlert(WebUser::ALERT_SUCCESS, 'Seu solicitação foi enviado com sucesso. Por favor, aguarde nosso contato.');

				if (isset($_POST['returnUrl']))
					$this->redirect($_POST['returnUrl']);
				else
					$this->redirect(array('index'));
			} else
				Yii::app()->user->setAlert(WebUser::ALERT_DANGER, current(current($franchiseContactForm->getErrors())));
		}

		$this->render('franchise', array(
			'franchiseApplicationForm' => $franchiseApplicationForm,
			'franchiseContactForm' => $franchiseContactForm,
		));
	}

	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
}

