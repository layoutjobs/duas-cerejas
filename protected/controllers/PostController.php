<?php

class PostController extends Controller
{
	public function actions()
	{
		return array(
			'postSubmitionUpload' => array(
				'class' => 'common.actions.UploadAction',
				'path' => PostSubmitionForm::model()->getUploadPath('gallery'),
				'url' => PostSubmitionForm::model()->getUploadUrl('gallery'),
				'options' => array(
					'accept_file_types' => PostSubmitionForm::model()->getUploadScopeAcceptFileTypes('gallery'),
					'image_versions' => Yii::app()->params['upload.imageVersions'],
				),
			),
		);
	}
		
	public function actionIndex($category = null, $id = null) 
	{
		$categories = Category::model()
			->of('posts:published:public')
			->notEmpty()
			->findAll(array(
				'index' => 'id',
				'order' => 't.sequence',
			));

		$criteria = new CDbCriteria;
		$currentCategory = null;

		if ($category !== null) {
			$categoryId = $category;
			
			foreach ($categories as $category) {
				if ($category->alias == $categoryId || $category->id == $categoryId) {
					$currentCategory = $category;
					break;
				}
			}

			if ($currentCategory === null)
				throw new CHttpException(404, 'Página não encontrada.');	

			$criteria->with[] = 'categories';
			$criteria->together = true;
			$criteria->compare('categories.id', $currentCategory->id);
		}

		$criteria->scopes[] = 'published';
		$criteria->scopes[] = 'public';
		$criteria->order = 't.publish_date DESC, t.title';
		$count = Post::model()->count($criteria);
		$pagination = new CPagination($count);
		$pagination->pageSize = 10;
		$pagination->applyLimit($criteria);
		$posts = Post::model()->findAll($criteria);

		$this->render('index', array(
			'currentCategory' => $currentCategory, 
			'categories' => $categories, 
			'pagination' => $pagination,
			'posts' => $posts, 
		));
	}

	public function actionView($id) 
	{
		$post = Post::model()->public()->published()->find(array(
			'condition' => 'id = :id OR alias = :id',
			'params' => array('id' => $id),
		));

		if ($post === null)
			throw new CHttpException(404, 'Página não encontrada.');	

		$this->render('view', array(
			'post' => $post, 
		));
	}

	public function actionSubmit()
	{
		$postSubmitionForm = new PostSubmitionForm;
		
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'postSubmitionForm') {
			echo CActiveForm::validate($postSubmitionForm);
			Yii::app()->end();
		}
		
		if (isset($_POST['PostSubmitionForm'])) {
			$postSubmitionForm->setAttributes($_POST['PostSubmitionForm']);

			if ($postSubmitionForm->save()) {
				Yii::app()->user->setAlert(WebUser::ALERT_SUCCESS, 'Sua postagem foi enviada com sucesso. Por favor, aguarde nosso retorno.');
				$this->redirect($this->createUrl('index'));
			} else
				Yii::app()->user->setAlert(WebUser::ALERT_DANGER, current(current($postSubmitionForm->getErrors())));
		}

		$this->render('submit', array('postSubmitionForm' => $postSubmitionForm));
	}

	public function createViewUrl($model)
	{
		if ($model instanceof Post)
			return $this->createUrl('view', array('id' => $model->alias));
		elseif ($model instanceof Category)
			return $this->createUrl('index', array('category' => $model->alias));
	}
}

