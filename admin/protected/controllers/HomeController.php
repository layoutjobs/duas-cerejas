<?php

class HomeController extends Controller
{
	public function accessRules()
	{
		return array(
			array(
				'allow',
				'actions' => array('index', 'logout', 'post', 'postView'),
				'users' => array('@'),
			),
			array(
				'allow',
				'actions' => array('error', 'login'),
				'users' => array('*'),
			),
			array('deny')
		);
	}	
		
	public function actionIndex()
	{
		if (!Yii::app()->user->is('guest'))
			$this->redirect($this->createUrl('post'));
	}
	
	public function actionPost($category = null, $id = null)
	{
		$categories = Category::model()
			->of('posts:published:private')
			->notEmpty()
			->findAll(array(
				'index' => 'id',
				'order' => 't.sequence',
			));

		$criteria = new CDbCriteria;
		$currentCategory = null;

		if ($category !== null) {
			$categoryId = $category;
			
			foreach ($categories as $category) {
				if ($category->alias == $categoryId || $category->id == $categoryId) {
					$currentCategory = $category;
					break;
				}
			}

			if ($currentCategory === null)
				throw new CHttpException(404, 'Página não encontrada.');	

			$criteria->with[] = 'categories';
			$criteria->together = true;
			$criteria->compare('categories.id', $currentCategory->id);
		}

		$criteria->scopes[] = 'published';
		$criteria->scopes[] = 'private';
		$criteria->order = 't.publish_date DESC, t.title';
		$count = Post::model()->count($criteria);
		$pagination = new CPagination($count);
		$pagination->pageSize = 10;
		$pagination->applyLimit($criteria);
		$posts = Post::model()->findAll($criteria);

		$this->render('post', array(
			'currentCategory' => $currentCategory, 
			'categories' => $categories, 
			'pagination' => $pagination,
			'posts' => $posts, 
		));		
	}

	public function actionPostView($id) 
	{
		$post = Post::model()->private()->published()->find(array(
			'condition' => 'id = :id OR alias = :id',
			'params' => array('id' => $id),
		));

		if ($post === null)
			throw new CHttpException(404, 'Página não encontrada.');	

		$this->render('postView', array(
			'post' => $post, 
		));
	}

	public function createPostViewUrl($model)
	{
		if ($model instanceof Post)
			return $this->createUrl('postView', array('id' => $model->alias));
		elseif ($model instanceof Category)
			return $this->createUrl('post', array('category' => $model->alias));
	}

	public function actionError()
	{
		if ($error = Yii::app()->errorHandler->error) {
			if (Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}
	
	public function actionLogin()
	{
		$model = new LoginForm;
		
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'loginForm') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		
		if (isset($_POST['LoginForm'])) {
			$model->attributes = $_POST['LoginForm'];
			if ($model->validate() && $model->login())
				$this->redirect(Yii::app()->user->returnUrl);
		}
		
		$this->render('login', array('model' => $model));
	}
	
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}
