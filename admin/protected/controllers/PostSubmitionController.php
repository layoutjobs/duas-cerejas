<?php

class PostSubmitionController extends Controller
{		
	public $searchForm = true;

	public function actions()
	{
		return array(
			'galleryUploadHandler' => array(
				'class' => 'common.actions.UploadAction',
				'path' => PostSubmition::model()->getUploadBasePath('img'),
				'url' => PostSubmition::model()->getUploadBaseUrl('img'),
				'options' => array(
					'accept_file_types' => PostSubmition::model()->getUploadScopeAcceptFileTypes('gallery'),
					'image_versions' => Yii::app()->params['upload.imageVersions'],
				),
			),
		);
	}
	
	public function actionIndex($s = null)
	{
		$model = new PostSubmition('search');
		$model->unsetAttributes();
		
		if (isset($_GET['PostSubmition']))
			$model->setAttributes($_GET['PostSubmition']);
		else 
			$model->s = $s;
		
		$this->render('index', array('model' => $model));
	}
	
	public function actionCreate()
	{
		$model = new PostSubmition('aproval');

		if(isset($_POST['PostSubmition'])) {
			$model->setAttributes($_POST['PostSubmition']);

			if ($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');

				$this->redirect(array('update', 'id' => $model->id));
			}
		}

		$this->render('create', array('model' => $model));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel('PostSubmition', $id, 'approval');

		if (isset($_POST['PostSubmition'])) {
			$model->setAttributes($_POST['PostSubmition']);

			if ($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');

				$this->refresh();
			}
		}

		$this->render('update', array('model' => $model));
	}

	public function actionGallery($id)
	{
		$model = $this->loadModel('PostSubmition');

		$this->render('gallery', array('model' => $model));
	}

	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$this->loadModel('PostSubmition', $id)->delete();

			if (!Yii::app()->request->isAjaxRequest) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> O registro foi excluído com sucesso.');		

				$this->redirect(array('index'));
			}
		} else
			throw new CHttpException(400, 'Requisição inválida. Por favor, não repita esta requisição novamente.');
	}
}
