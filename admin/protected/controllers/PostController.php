<?php

class PostController extends Controller
{		
	public $searchForm = true;

	public function actions()
	{
		return array(
			'galleryUploadHandler' => array(
				'class' => 'common.actions.UploadAction',
				'path' => Post::model()->getUploadBasePath('img'),
				'url' => Post::model()->getUploadBaseUrl('img'),
				'options' => array(
					'accept_file_types' => Post::model()->getUploadScopeAcceptFileTypes('gallery'),
					'image_versions' => Yii::app()->params['upload.imageVersions'],
				),
			),
		);
	}
	
	public function actionIndex($s = null, $private = null)
	{
		$model = new Post('search');
		$model->unsetAttributes();

		if ($private != null) 
			Yii::app()->user->setState('post.private', $private);
	
		if (isset($_GET['Post']))
			$model->setAttributes($_GET['Post']);
		else 
			$model->s = $s;

		$model->private = Yii::app()->user->getState('post.private', $private);
		
		$this->render('index', array('model' => $model));
	}
	
	public function actionCreate()
	{
		$model = new Post;
		$model->private = Yii::app()->user->getState('post.private');

		if (isset($_POST['Post'])) {
			$model->setAttributes($_POST['Post']);
			$img = CUploadedFile::getInstance($model, 'img');
			$model->img = $img ? $img : $model->img;

			if ($model->saveWithRelated(array('categories'))) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');

				$this->redirect(array('update', 'private' => Yii::app()->user->getState('post.private'), 'id' => $model->id));
			}
		}

		$this->render('create', array('model' => $model));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel('Post');

		if (isset($_POST['Post'])) {
			$model->setAttributes($_POST['Post']);
			$img = CUploadedFile::getInstance($model, 'img');
			$model->img = $img ? $img : $model->img;

			if ($model->saveWithRelated(array('categories'))) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');

				$this->refresh();
			}
		}

		$this->render('update', array('model' => $model));
	}

	public function actionGallery($id)
	{
		$model = $this->loadModel('Post');

		$this->render('gallery', array('model' => $model));
	}

	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$this->loadModel('Post', $id)->delete();

			if (!Yii::app()->request->isAjaxRequest) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> O registro foi excluído com sucesso.');		

				$this->redirect(array('index', 'private' => Yii::app()->user->getState('post.private')));
			}
		} else
			throw new CHttpException(400, 'Requisição inválida. Por favor, não repita esta requisição novamente.');
	}
}
