<?php

class CategoryController extends Controller
{		
	public $searchForm = true;
	
	public function actionIndex($belongsTo, $s = null)
	{
		$model = new Category('search');
		$model->unsetAttributes();

		if ($belongsTo !== 'Product' && $belongsTo !== 'Post')
			throw new CHttpException(400, 'Requisição inválida. Por favor, não repita esta requisição novamente.');

		$model->belongs_to = $belongsTo;
			
		if (isset($_GET['Category']))
			$model->setAttributes($_GET['Category']);
		else 
			$model->s = $s;
		
		$this->render('index', array('model' => $model));
	}
	
	public function actionCreate($belongsTo)
	{
		$model = new Category;

		if ($belongsTo !== 'Product' && $belongsTo !== 'Post')
			throw new CHttpException(400, 'Requisição inválida. Por favor, não repita esta requisição novamente.');

		$model->belongs_to = $belongsTo;

		if (isset($_POST['Category'])) {
			$model->setAttributes($_POST['Category']);
			$img = CUploadedFile::getInstance($model, 'img');
			$model->img = $img ? $img : $model->img;

			if ($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');

				$this->redirect(array('update', 'id' => $model->id));
			}
		}

		$this->render('create', array('model' => $model));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel('Category');

		if (isset($_POST['Category'])) {
			$model->setAttributes($_POST['Category']);
			$img = CUploadedFile::getInstance($model, 'img');
			$model->img = $img ? $img : $model->img;

			if ($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');

				$this->refresh();
			}
		}

		$this->render('update', array('model' => $model));
	}
	
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$this->loadModel('Category', $id)->delete();

			if (!Yii::app()->request->isAjaxRequest) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> O registro foi excluído com sucesso.');		

				$this->redirect(array('index'));
			}
		} else
			throw new CHttpException(400, 'Requisição inválida. Por favor, não repita esta requisição novamente.');
	}
}
