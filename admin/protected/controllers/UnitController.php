<?php

class UnitController extends Controller
{		
	public $searchForm = true;
	
	public function actionIndex($s = null)
	{
		$model = new Unit('search');
		$model->unsetAttributes();
		
		if (isset($_GET['Unit']))
			$model->setAttributes($_GET['Unit']);
		else 
			$model->s = $s;
		
		$this->render('index', array('model' => $model));
	}
	
	public function actionCreate()
	{
		$model = new Unit;

		if(isset($_POST['Unit'])) {
			$model->setAttributes($_POST['Unit']);
			$img = CUploadedFile::getInstance($model, 'img');
			$model->img = $img ? $img : $model->img;

			if ($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');

				$this->redirect(array('update', 'id' => $model->id));
			}
		}

		$this->render('create', array('model' => $model));
	}

	public function actionUpdate($id)
	{
		$model = $this->loadModel('Unit');

		if (isset($_POST['Unit'])) {
			$model->setAttributes($_POST['Unit']);
			$img = CUploadedFile::getInstance($model, 'img');
			$model->img = $img ? $img : $model->img;

			if ($model->save()) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> As alterações foram salvas com sucesso.');

				$this->refresh();
			}
		}

		$this->render('update', array('model' => $model));
	}
	
	public function actionDelete($id)
	{
		if (Yii::app()->request->isPostRequest) {
			$this->loadModel('Unit', $id)->delete();

			if (!Yii::app()->request->isAjaxRequest) {
				Yii::app()->user->setFlash(TbHtml::ALERT_COLOR_SUCCESS, 
					'<strong>Sucesso!</strong> O registro foi excluído com sucesso.');		

				$this->redirect(array('index'));
			}
		} else
			throw new CHttpException(400, 'Requisição inválida. Por favor, não repita esta requisição novamente.');
	}
}
