<?php 
$this->pageTitle = 'Banners';
$this->menu = array(
	array(
		array(
			'label' => 'Incluir',
			'icon' => 'file',
			'url' => array('create'),
		),
	),
); ?>

<?php $this->widget('yiiwheels.widgets.grid.WhGroupGridView', array(
	'type' => array(TbHtml::GRID_TYPE_STRIPED, TbHtml::GRID_TYPE_BORDERED),
	'dataProvider' => $model->search(),
	'extraRowColumns' => array('position'),
	'columns' => array(
		array(
			'name' => 'position',
			'htmlOptions' => array('style' => 'display: none;'),
			'headerHtmlOptions' => array('style' => 'display: none;'),
		),
		array(
			'name' => 'img',
			'type' => 'raw',
			'value' => 'CHtml::image($data->getImage("thumbnail")->url, null, array("style" => "height: 40px; width: auto;"))',
		),
		'name',
		'url:url',
		'sequence',
		array(
			'name' => 'routes',
			'value' => '$data->routeAsOptionsLabels',
		),
		array(
			'name' => 'active',
			'type' => 'raw',
			'value' => '$data->active ? TbHtml::icon(TbHtml::ICON_OK) : null',
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => "{update}\n{delete}",
		),
	),
)); ?>