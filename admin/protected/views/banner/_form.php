<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="control-group<?php echo $model->hasErrors('img') ? ' error' : null; ?>">
	<?php echo $form->labelEx($model, 'img', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php if (($thumbnail = $model->getImage('thumbnail')) !== null) : ?>

		<span class="file">
			<img class="img-polaroid" src="<?php echo $thumbnail->url; ?>" style="height: 100px; width: auto;">
			<?php echo $form->fileField($model, 'img'); ?>	
		</span>	

		<?php else : ?>	

		<span class="file">
			<?php echo TbHtml::button('Selecionar'); ?>			
			<?php echo $form->fileField($model, 'img'); ?>	
		</span>	

		<?php endif; ?>

		<?php echo $form->error($model, 'img'); ?>
	</div>
</div>	

<?php echo $form->dropDownListControlGroup($model, 'active', array(1 => 'Ativo', 0 => 'Inativo'), array('empty' => '')); ?>

<?php echo $form->textFieldControlGroup($model, 'name'); ?>

<?php echo $form->textFieldControlGroup($model, 'sequence'); ?>

<?php echo $form->textFieldControlGroup($model, 'url'); ?>

<?php echo $form->dropDownListControlGroup($model, 'position', $model->positionOptions, array('empty' => '')); ?>

<div class="control-group<?php echo $model->hasErrors('routes') ? ' error' : null; ?>">
	<?php echo $form->labelEx($model, 'routes', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php $model->routes = $model->routeAsArray; ?>

		<?php $this->widget('yiiwheels.widgets.multiselect.WhMultiSelect', array(
		    'model' => $model,
		    'attribute' => 'routes',
		    'data' => $model->routeOptions,
		)); ?>
		<?php echo $form->error($model, 'routes'); ?>
	</div>
</div>

<div class="form-actions">
	<?php echo TbHtml::button('Salvar', array(
		'type' => 'submit',
		'color' => TbHtml::BUTTON_COLOR_PRIMARY,
		'icon' => TbHtml::ICON_OK,
	)); ?>	

	<?php echo TbHtml::linkButton('Cancelar', array(
		'type' => 'link',
		'url' => array('index'),
		'color' => TbHtml::BUTTON_COLOR_LINK,
	)); ?>
</div>
	
<?php $this->endWidget(); ?>