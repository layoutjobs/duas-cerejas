<?php 
$this->pageTitle = 'Publicações Enviadas';
$this->menu = array(
	array(
		array(
			'label' => 'Incluir',
			'icon' => 'file',
			'url' => array('create'),
		),
	),
); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type' => array(TbHtml::GRID_TYPE_STRIPED, TbHtml::GRID_TYPE_BORDERED),
	'dataProvider' => $model->search(),
	'columns' => array(
		'date',
		'sender_name',
		'title',
		array(
			'name' => 'excerpt',
			'htmlOptions' => array('width' => '50%'),
		),
		array(
			'name' => 'published',
			'type' => 'raw',
			'value' => '$data->status ? TbHtml::icon(TbHtml::ICON_OK) . \' \' . $data->publish_date : null',
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => "{update}\n{delete}",
		),
	),
)); ?>