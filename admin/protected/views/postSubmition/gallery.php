<?php 
$this->pageTitle = 'Publicação Enviada';
$this->pageSubtitle = 'Upload (' . CHtml::encode($model->title) . ')';
$this->menu = array(
	array(
		array(
			'label' => 'OK',
			'icon' => TbHtml::ICON_OK,
			'url' => $this->createUrl('update', array('id' => $model->id)),
		),
	),
); ?>

<p><?php echo TbHtml::icon(TbHtml::ICON_INFO_SIGN); ?> Evite enviar images com nomes contendo 
espaços, acentos ou caracteres especiais (Ç, !, ?, etc.), assim como nomes muito  longos 
(máximo 128 caracteres). São aceitos arquivos com as seguintes extensões: 
<strong>.gif, .jpg e .png</strong>.</p> 

<?php $this->widget('application.widgets.upload.Upload', array(
	'url' => $this->createUrl('galleryUploadHandler', array('folder' => $model->getUploadModelFolder())),
)); ?>
