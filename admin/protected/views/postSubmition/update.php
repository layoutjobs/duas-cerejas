<?php 
$this->pageTitle = 'Publicação Enviada';
$this->pageSubtitle = 'Editar (' . CHtml::encode($model->title) . ')';
$this->menu = array(
	array(
		array(
			'label' => 'Galeria',
			'icon' => TbHtml::ICON_PICTURE,
			'url' => $this->createUrl('gallery', array('id' => $model->id)),
		),
		array(
			'label' => 'Excluir',
			'icon' => TbHtml::ICON_TRASH,
			'color' => TbHtml::BUTTON_COLOR_DANGER,
			'url' => '#',
			'htmlOptions' => array(
				'submit' => array('delete', 'id' => $model->id),
				'confirm' => 'Tem certeza que deseja excluir este registro?',
			),
		),
	),
); ?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>