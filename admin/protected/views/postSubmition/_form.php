<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="control-group<?php echo $model->hasErrors('categories') ? ' error' : null; ?>">
	<?php echo $form->labelEx($model, 'categories', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php $this->widget('yiiwheels.widgets.multiselect.WhMultiSelect', array(
		    'model' => $model,
		    'attribute' => 'categories',
		    'data' => $model->categoryOptions,
		)); ?>
		<?php echo $form->error($model, 'categories'); ?>
	</div>
</div>

<?php echo $form->dropDownListControlGroup($model, 'status', $model->statusOptions); ?>

<div class="control-group<?php echo $model->hasErrors('publish_date') ? ' error' : null; ?>">
	<?php echo $form->labelEx($model, 'publish_date', array('class' => 'control-label')); ?>
	<div class="controls">
		<div class="input-append">
			<?php $this->widget('yiiwheels.widgets.datepicker.WhDatePicker', array(
			    'model' => $model,
			    'attribute' => 'publish_date',
				'pluginOptions' => array(
					'format' => 'dd/mm/yyyy',
					'language' => 'pt',
				),
			)); ?>
		    <span class="add-on"><?php echo TbHtml::icon(TbHtml::ICON_CALENDAR); ?></span>
		</div>
		<?php echo $form->error($model, 'publish_date'); ?>
	</div>
</div>

<?php echo $form->textFieldControlGroup($model, 'title'); ?>

<div class="control-group<?php echo $model->hasErrors('content') ? ' error' : null; ?>">
	<?php echo $form->labelEx($model, 'content', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
		    'model' => $model,
		    'attribute' => 'content',
		)); ?>
		<?php echo $form->error($model, 'content'); ?>
	</div>
</div>

<?php echo $form->textFieldControlGroup($model, 'sender_name'); ?>

<?php echo $form->textFieldControlGroup($model, 'sender_email'); ?>

<?php echo $form->textFieldControlGroup($model, 'sender_friends_emails'); ?>

<?php echo $form->checkBoxControlGroup($model, 'notify_sender'); ?>

<?php echo $form->textAreaControlGroup($model, 'info'); ?>

<div class="form-actions">
	<?php echo TbHtml::button('Salvar', array(
		'type' => 'submit',
		'color' => TbHtml::BUTTON_COLOR_PRIMARY,
		'icon' => TbHtml::ICON_OK,
	)); ?>	

	<?php echo TbHtml::linkButton('Cancelar', array(
		'type' => 'link',
		'url' => array('index'),
		'color' => TbHtml::BUTTON_COLOR_LINK,
	)); ?>
</div>
	
<?php $this->endWidget(); ?>