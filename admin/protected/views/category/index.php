<?php 
$this->pageTitle = 'Categorias';
$this->menu = array(
	array(
		array(
			'label' => 'Incluir',
			'icon' => 'file',
			'url' => array('create', 'belongsTo' => $model->belongs_to),
		),
	),
); 
$this->searchParams = array('belongsTo' => $model->belongs_to);
?>

<?php $this->widget('yiiwheels.widgets.grid.WhGroupGridView', array(
	'type' => array(TbHtml::GRID_TYPE_BORDERED),
	'dataProvider' => $model->search(),
	'extraRowColumns' => array('parent_id'),
	'columns' => array(
		'name',
		array(
			'name' => 'description',
			'htmlOptions' => array('width' => '40%'),
		),
		'sequence',
		array(
			'name' => 'img',
			'type' => 'raw',
			'value' => '$data->image ? CHtml::image($data->getImage("thumbnail")->url, null, array("style" => "height: 100%; width: auto;")) : ""',
			'htmlOptions' => array('style' => 'height: 40px;'),			
		),	
		array(
			'name' => 'parent_id',
			'type' => 'raw',
			'value' => '$data->parent !== null ? $data->parent->name . " " . (CHtml::link(TbHtml::icon(TbHtml::ICON_PENCIL), Yii::app()->controller->createUrl("update", array("id" => $data->parent_id)))) : "Categorias Pai"',
			'htmlOptions' => array('style' => 'display: none;'),
			'headerHtmlOptions' => array('style' => 'display: none;'),
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => "{update}\n{delete}",
		),
	),
)); ?>