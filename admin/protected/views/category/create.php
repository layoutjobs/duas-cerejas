<?php 
$this->pageTitle = 'Categoria';
$this->pageSubtitle = 'Incluir';
$this->searchParams = array('belongsTo' => $model->belongs_to);
?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
