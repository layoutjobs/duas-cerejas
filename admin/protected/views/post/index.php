<?php 
$this->pageTitle = 'Publicações' . ($model->private ? ' Privadas' : '');
$this->menu = array(
	array(
		array(
			'label' => 'Incluir',
			'icon' => 'file',
			'url' => array('create'),
		),
	),
); 
$this->searchParams = array('private' => $model->private);
?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type' => array(TbHtml::GRID_TYPE_STRIPED, TbHtml::GRID_TYPE_BORDERED),
	'dataProvider' => $model->search(),
	'columns' => array(
		array(
			'name' => 'img',
			'type' => 'raw',
			'value' => '$data->getImage("thumbnail") ? CHtml::image($data->getImage("thumbnail")->url, null, array("style" => "height: 40px; width: auto;")) : null',
		),
		'publish_date',
		'title',
		array(
			'name' => 'categories',
			'value' => "implode(', ', CHtml::listData(\$data->categories, 'id', 'name'))",
		),
		array(
			'name' => 'excerpt',
			'htmlOptions' => array('width' => '50%'),
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => "{update}\n{delete}",
		),
	),
)); ?>