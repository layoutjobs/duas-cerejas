<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="control-group<?php echo $model->hasErrors('img') ? ' error' : null; ?>">
	<?php echo $form->labelEx($model, 'img', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php if (($thumbnail = $model->getImage('thumbnail')) !== null) : ?>

		<span class="file">
			<img class="img-polaroid" src="<?php echo $thumbnail->url; ?>" style="height: 100px; width: auto;">
			<?php echo $form->fileField($model, 'img'); ?>	
		</span>	

		<?php else : ?>	

		<span class="file">
			<?php echo TbHtml::button('Selecionar'); ?>			
			<?php echo $form->fileField($model, 'img'); ?>	
		</span>	

		<?php endif; ?>

		<?php echo $form->error($model, 'img'); ?>
	</div>
</div>	

<div class="control-group<?php echo $model->hasErrors('categories') ? ' error' : null; ?>">
	<?php echo $form->labelEx($model, 'categories', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php $this->widget('yiiwheels.widgets.multiselect.WhMultiSelect', array(
		    'model' => $model,
		    'attribute' => 'categories',
		    'data' => $model->categoryOptions,
		)); ?>
		<?php echo $form->error($model, 'categories'); ?>
	</div>
</div>

<?php echo $form->dropDownListControlGroup($model, 'published', array(1 => 'Publicado', 0 => 'Não Publicado')); ?>

<?php echo $form->dropDownListControlGroup($model, 'private', array(1 => 'Privado', 0 => 'Não Privado')); ?>

<div class="control-group<?php echo $model->hasErrors('publish_date') ? ' error' : null; ?>">
	<?php echo $form->labelEx($model, 'publish_date', array('class' => 'control-label')); ?>
	<div class="controls">
		<div class="input-append">
			<?php $this->widget('yiiwheels.widgets.datepicker.WhDatePicker', array(
			    'model' => $model,
			    'attribute' => 'publish_date',
				'pluginOptions' => array(
					'format' => 'dd/mm/yyyy',
					'language' => 'pt',
				),
			)); ?>
		    <span class="add-on"><?php echo TbHtml::icon(TbHtml::ICON_CALENDAR); ?></span>
		</div>
		<?php echo $form->error($model, 'publish_date'); ?>
	</div>
</div>

<?php echo $form->textFieldControlGroup($model, 'title'); ?>
<div class="control-group<?php echo $model->hasErrors('content') ? ' error' : null; ?>">
	<?php echo $form->labelEx($model, 'content', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php $this->widget('yiiwheels.widgets.redactor.WhRedactor', array(
		    'model' => $model,
		    'attribute' => 'content',
		)); ?>
		<?php echo $form->error($model, 'content'); ?>
	</div>
</div>

<?php echo $form->textAreaControlGroup($model, 'excerpt', array('style' => 'height: 160px;')); ?>

<?php echo $form->dropDownListControlGroup($model, 'notify', $model->notifyOptions, array('empty' => 'Não Notificar')); ?>

<div class="form-actions">
	<?php echo TbHtml::button('Salvar', array(
		'type' => 'submit',
		'color' => TbHtml::BUTTON_COLOR_PRIMARY,
		'icon' => TbHtml::ICON_OK,
	)); ?>	

	<?php echo TbHtml::linkButton('Cancelar', array(
		'type' => 'link',
		'url' => array('index', 'private' => $model->private),
		'color' => TbHtml::BUTTON_COLOR_LINK,
	)); ?>
</div>
	
<?php $this->endWidget(); ?>