<?php 
$this->pageTitle = 'Publicação';
$this->pageSubtitle = 'Incluir';
$this->searchParams = array('private' => $model->private);
?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>
