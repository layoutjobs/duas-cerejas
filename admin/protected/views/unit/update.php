<?php 
$this->pageTitle = 'Unidade';
$this->pageSubtitle = 'Editar (' . CHtml::encode($model->name) . ')';
$this->menu = array(
	array(
		array(
			'label' => 'Excluir',
			'icon' => TbHtml::ICON_TRASH,
			'color' => TbHtml::BUTTON_COLOR_DANGER,
			'url' => '#',
			'htmlOptions' => array(
				'submit' => array('delete', 'id' => $model->id),
				'confirm' => 'Tem certeza que deseja excluir este registro?',
			),
		),
	),
); ?>

<?php echo $this->renderPartial('_form', array('model' => $model)); ?>