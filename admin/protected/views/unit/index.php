<?php 
$this->pageTitle = 'Unidades';
$this->menu = array(
	array(
		array(
			'label' => 'Incluir',
			'icon' => 'file',
			'url' => array('create'),
		),
	),
); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'type' => array(TbHtml::GRID_TYPE_STRIPED, TbHtml::GRID_TYPE_BORDERED),
	'dataProvider' => $model->search(),
	'columns' => array(
		array(
			'name' => 'img',
			'type' => 'raw',
			'value' => 'CHtml::image($data->getImage("thumbnail")->url, null, array("style" => "height: 40px; width: auto;"))',
		),
		'city',
		'name',
		'about',
		'user.username',
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => "{update}\n{delete}",
		),
	),
)); ?>