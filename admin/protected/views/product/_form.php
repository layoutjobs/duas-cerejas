<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
	'htmlOptions' => array(
		'enctype' => 'multipart/form-data',
	),
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="control-group<?php echo $model->hasErrors('img') ? ' error' : null; ?>">
	<?php echo $form->labelEx($model, 'img', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php if (($thumbnail = $model->getImage('thumbnail')) !== null) : ?>

		<span class="file">
			<img class="img-polaroid" src="<?php echo $thumbnail->url; ?>" style="height: 100px; width: auto;">
			<?php echo $form->fileField($model, 'img'); ?>	
		</span>	

		<?php else : ?>	

		<span class="file">
			<?php echo TbHtml::button('Selecionar'); ?>			
			<?php echo $form->fileField($model, 'img'); ?>	
		</span>	

		<?php endif; ?>

		<?php echo $form->error($model, 'img'); ?>
	</div>
</div>	

<?php echo $form->textFieldControlGroup($model, 'name'); ?>

<?php echo $form->dropDownListControlGroup($model, 'sweetness', array(0 => 'Sem doçura', 1 => '1', 2 => '2')); ?>

<div class="control-group<?php echo $model->hasErrors('categories') ? ' error' : null; ?>">
	<?php echo $form->labelEx($model, 'categories', array('class' => 'control-label')); ?>
	<div class="controls">
		<?php $this->widget('yiiwheels.widgets.multiselect.WhMultiSelect', array(
		    'model' => $model,
		    'attribute' => 'categories',
		    'data' => $model->categoryOptions,
		)); ?>
		<?php echo $form->error($model, 'categories'); ?>
	</div>
</div>

<?php echo $form->textAreaControlGroup($model, 'description', array('style' => 'height: 200px;')); ?>

<div class="form-actions">
	<?php echo TbHtml::button('Salvar', array(
		'type' => 'submit',
		'color' => TbHtml::BUTTON_COLOR_PRIMARY,
		'icon' => TbHtml::ICON_OK,
	)); ?>	

	<?php echo TbHtml::linkButton('Cancelar', array(
		'type' => 'link',
		'url' => array('index'),
		'color' => TbHtml::BUTTON_COLOR_LINK,
	)); ?>
</div>
	
<?php $this->endWidget(); ?>