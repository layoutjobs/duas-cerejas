<?php 
$this->pageTitle = 'Blog';
$this->pageSubtitle = 'Fique por dentro das novidades Duas Cerejas.'; 
?>

<div class="row-fluid">
	<div class="span8">
		<?php foreach ($posts as $post) : ?>

		<div class="panel">
			<article class="post">
				<h3 style="margin-top: 0;">
					<?php echo CHtml::encode($post->title); ?>
					<small class="post-meta">
						<?php $publishDate = new DateTime($post->parseDateTime('publish_date')); ?> 			
						<time class="post-publish-date" datetime="<?php echo $publishDate->format('Y-m-d'); ?>">
							<?php echo $publishDate->format('d/m/Y'); ?>
						</time>
					</small>
				</h3>

				<?php if ($post->image) : ?>

				<figure class="post-img">
					<img src="<?php echo $post->image->url; ?>">
				</figure>
				
				<?php endif; ?>

				<p class="post-excerpt"><?php echo $post->excerpt; ?> <a href="<?php echo $this->createPostViewUrl($post); ?>">[ + ] Continue Lendo</a></p>

				<?php if ($gallery = $post->getFiles('gallery', 'large')) : ?>

				<div class="post-gallery owl-carousel">
					<?php foreach ($gallery as $image) : ?>

					<a href="<?php echo $image->url; ?>" rel="prettyPhoto[<?php echo $post->id; ?>]" class="item"><img src="<?php echo $image->url; ?>"></a>

					<?php endforeach; ?>
				</div>

				<?php endif; ?>
			</article>
		</div>

		<?php endforeach; ?>

		<?php if ($pagination->pageCount > 1) : ?>

		<ul class="pager">

			<?php if ($pagination->currentPage < ($pagination->pageCount - 1))  : ?>

			<li class="previous"><a href="<?php echo $pagination->createPageUrl($this, $pagination->currentPage + 1); ?>">&laquo; Mais Antigos</a></li>

			<?php endif; ?>

			<?php if ($pagination->currentPage > 0)  : ?>

			<li class="next"><a href="<?php echo $pagination->createPageUrl($this, ($pagination->currentPage - 1)); ?>">Mais Recentes &raquo;</a></li>

			<?php endif; ?>
		</ul>

		<?php endif; ?>

	</div>
	<div class="span4">
		<div class="well" style="padding-left: 0; padding-right: 0;">
			<ul class="nav nav-list">
				<li class="nav-header">Categorias</li>

				<?php foreach ($categories as $category) : ?>

				<li<?php echo $currentCategory && $category->id === $currentCategory->id ? ' class="active"' : ''; ?>>
					<a href="<?php echo $this->createPostViewUrl($category); ?>"><?php echo CHtml::encode($category->name); ?></a>
				</li>

				<?php endforeach; ?>
			</ul>
		</div>
	</div>
</div>

<?php 
$js = <<<JS
$('.post-gallery.owl-carousel').owlCarousel({
    loop: true,
    items: 2,
    margin: 5,
	autoplay: true,
    autoplayTimeout: 8000,
    autoplayHoverPause: true,
	responsive: {
		768: {
		    items: 3
		},
		992: {
		    items: 3
		},
		1200: {
		    items: 4
		}
	}
});

$("a[rel^='prettyPhoto']").prettyPhoto();
JS;
Yii::app()->clientScript->registerPackage('owl.carousel'); 
Yii::app()->clientScript->registerPackage('prettyPhoto'); 
Yii::app()->clientScript->registerScript('home.posts', $js, CClientScript::POS_END); 
?>