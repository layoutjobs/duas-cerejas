<?php 
$this->pageTitle = CHtml::encode($post->title);
$this->pageSubtitle = 'Postado em ' . $post->publish_date;
?>

<div class="row-fluid">
	<div class="span8 offset2">
		<article class="post">
			<h2 class="post-title"><?php echo CHtml::encode($post->title); ?></h2>
			<p class="post-meta">
				<?php $publishDate = new DateTime($post->parseDateTime('publish_date')); ?> 			
				<time class="post-publish-date" datetime="<?php echo $publishDate->format('Y-m-d'); ?>">
					<i class="fa fa-clock-o"></i>
					<?php echo $publishDate->format('d/m/Y'); ?>
				</time>
				<span class="post-author">
					<i class="fa fa-user"></i>
					<?php echo $post->author->name; ?>
				</span>
			</p>

			<?php if ($post->image) : ?>

			<figure class="post-img">
				<img src="<?php echo $post->image->url; ?>">
			</figure>
			
			<?php endif; ?>

			<div class="post-content">
				<?php echo $post->content; ?>
			</div>

			<?php if ($gallery = $post->getFiles('gallery', 'large')) : ?>

			<div class="post-gallery owl-carousel">
				<?php foreach ($gallery as $image) : ?>

				<a href="<?php echo $image->url; ?>" rel="prettyPhoto[<?php echo $post->id; ?>]" class="item"><img src="<?php echo $image->url; ?>"></a>

				<?php endforeach; ?>
			</div>

			<?php endif; ?>
		</article>
	</div>
</div>

<?php 
$js = <<<JS
$('.post-gallery.owl-carousel').owlCarousel({
    loop: true,
    items: 2,
    margin: 5,
	autoplay: true,
    autoplayTimeout: 8000,
    autoplayHoverPause: true,
	responsive: {
		768: {
		    items: 3
		},
		992: {
		    items: 3
		},
		1200: {
		    items: 4
		}
	}
});

$("a[rel^='prettyPhoto']").prettyPhoto();
JS;
Yii::app()->clientScript->registerPackage('owl.carousel'); 
Yii::app()->clientScript->registerPackage('prettyPhoto'); 
Yii::app()->clientScript->registerScript('post.index', $js, CClientScript::POS_END); 
?>