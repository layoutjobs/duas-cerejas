<?php 
$this->pageTitle = 'Usuários';
$this->menu = array(
	array(
		array(
			'label' => 'Incluir',
			'icon' => 'file',
			'url' => array('create'),
		),
	),
); ?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'userGrid',
	'type' => array(TbHtml::GRID_TYPE_STRIPED, TbHtml::GRID_TYPE_BORDERED),
	'dataProvider' => $model->search(),
	'columns' => array(
		'id',
		'name',	
		'username',
		array(
			'name' => 'type',
			'value' => '$data->type . ($data->owner_id ? " (" . $data->owner_id . ")" : "")',
		),
		array(
			'class' => 'bootstrap.widgets.TbButtonColumn',
			'template' => "{update}\n{delete}",
		),
	),
)); ?>