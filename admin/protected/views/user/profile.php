<?php 
$this->pageTitle = 'Perfil';
$this->pageSubtitle = CHtml::encode($model->name);
$this->searchForm = false;
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id' => 'userForm',
	'layout' => TbHtml::FORM_LAYOUT_HORIZONTAL,
)); ?>

<?php echo $form->errorSummary($model); ?>

<div class="row-fluid">
	<div class="span6">
		<?php echo $form->passwordFieldControlGroup($model, 'newPassword'); ?>
	</div>
	<div class="span6">
		<?php echo $form->passwordFieldControlGroup($model, 'newPasswordRepeat'); ?>
	</div>
</div>

<div class="form-actions">
	<?php echo TbHtml::button('Salvar', array(
		'type' => 'submit',
		'color' => TbHtml::BUTTON_COLOR_PRIMARY,
		'icon' => TbHtml::ICON_OK,
	)); ?>	

	<?php echo TbHtml::linkButton('Cancelar', array(
		'type' => 'link',
		'url' => Yii::app()->homeUrl,
		'color' => TbHtml::BUTTON_COLOR_LINK,
	)); ?>
</div>
	
<?php $this->endWidget(); ?>