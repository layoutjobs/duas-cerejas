<?php
return array (
  'template' => 'default',
  'connectionId' => 'db',
  'tablePrefix' => '',
  'modelPath' => 'common.models',
  'baseClass' => 'GxActiveRecord',
  'buildRelations' => true,
  'commentsAsLabels' => false,
);
