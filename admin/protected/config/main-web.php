<?php

$params = require(__DIR__ . '/params-web.php');

return array(
	'modules' => array(
		'gii' => array(
			'class' => 'system.gii.GiiModule',
			'password' => 'admin',
			'generatorPaths' => array(
				'ext.giix-core', // giix generators
			),
		),
	),

	'params' => $params,
);