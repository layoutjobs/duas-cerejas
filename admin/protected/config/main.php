<?php

$base 		 = __DIR__ . '/..';
$common      = __DIR__ . '/../../../common';
$environment = APP_ENVIRONMENT;
$params      = require(__DIR__ . '/params.php');

return CMap::mergeArray(require($common . '/config/main.php'), array(
	'aliases' => array(
		'bootstrap' => 'common.extensions.yiistrap',
		'yiiwheels' => 'common.extensions.yiiwheels',
    ),
	
	'import' => array(
        'bootstrap.helpers.TbHtml',
	),
	
	'components' => array(
		'urlManager' => array(
			'urlFormat' => 'path',
			'showScriptName' => false, // requer .htaccess
			'rules' => array(
				/* for REST please @see http://www.yiiframework.com/wiki/175/how-to-create-a-rest-api/ */
				/* other @see http://www.yiiframework.com/doc/guide/1.1/en/topics.url */
				'<controller:\w+>/<id:\d+>' => '<controller>/view',
				'<controller:\w+>/<action:\w+>/<id>' => '<controller>/<action>',
				'<controller:\w+>/<action:\w+>' => '<controller>/<action>',
			),
		),
		'bootstrap' => array(
			'class' => 'bootstrap.components.TbApi',
		),
		'yiiwheels' => array(
			'class' => 'yiiwheels.YiiWheels',
		),
	),
	
	'params' => $params,
), require(__DIR__ . '/main-' . $environment . '.php'));