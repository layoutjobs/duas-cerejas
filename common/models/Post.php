<?php

class Post extends CActiveRecord
{	
	public $s;
	public $notify;
	public $postSubmition;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{post}}';
	}

	public function rules()
	{
		return array(
			array('title, categories, content', 'required'),
			array('title', 'length', 'max' => 128),
			array('title', 'unique'),
			array('published, private', 'boolean'),
			array('published', 'default', 'value' => 1, 'setOnEmpty' => true),
			array('publish_date', 'date', 'format' => Yii::app()->locale->dateFormat),
			array('publish_date', 'default', 'value' => $this->formatDateTime('publish_date', null), 'setOnEmpty' => true),
			array('private', 'default', 'value' => 0, 'setOnEmpty' => true),
			array('notify', 'in', 'range' => array_keys($this->notifyOptions)),
			array('author_id', 'default', 'value' => Yii::app()->user->id, 'setOnEmpty' => true),
			array('content', 'filter', 'filter' => array($purifier = new CHtmlPurifier(), 'purify')),
			array('img', 'file', 'types' => 'jpg, jpeg, gif, png', 'allowEmpty' => true),
			array('excerpt', 'safe'),

			array('s', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'categories' => array(self::MANY_MANY, 'Category', Yii::app()->db->tablePrefix . 'post_category(post_id, category_id)'),
			'author' => array(self::BELONGS_TO, 'User', 'author_id'),
			// 'postSubmition' => array(self::HAS_ONE, 'PostSubmition', 'post_id'),
		);
	}

	public function scopes()
	{
		return array(
			'published' => array(
				'condition' => 'published = 1',
			),
			'private' => array(
				'condition' => 'private = 1',
			),
			'public' => array(
				'condition' => 'private = 0',
			),
		);
	}

	public function defaultScope() 
	{
		$alias = $this->getTableAlias(false, false);
		return array('order' => "$alias.published, $alias.publish_date DESC, $alias.title");
	}
	
	public function attributeLabels()
	{
		return array(
			's' => 'Pesquisar',
			'id' => '#',
			'title' => 'Título',
			'img' => 'Imagem',
			'content' => 'Conteúdo',
			'excerpt' => 'Resumo',
			'categories' => 'Categorias',
			'published' => 'Publicado?',
			'publish_date' => 'Data Publicação',
			'private' => 'Privado',
			'notify' => 'Notificar',
		);
	}

	public function behaviors()
	{
		return array(
			'saveRelatedBehavior' => array(
				'class' => 'common.behaviors.ESaveRelatedBehavior',
			),
			'imageBehavior' => array(
				'class' => 'common.behaviors.ImageBehavior',
			),
			'aliasBehavior' => array(
				'class' => 'common.behaviors.AliasBehavior',
				'aliasOriginAttribute' => 'title',
			),
			'excerptBehavior' => array(
				'class' => 'common.behaviors.ExcerptBehavior',
			),
			'dateTimeBehavior' => array(
				'class' => 'common.behaviors.DateTimeBehavior',
			),
			'fileBehavior' => array(
				'class' => 'common.behaviors.FileBehavior',
				'uploadFolder' => 'post',
				'uploadModelFolder' => substr(md5($this->id), 0, 10),
				'uploadScopes' => array(
					'gallery' => array(
						'acceptFileTypes' => '/(\.|\/)(gif|jpe?g|png)$/i',
					),
				),
			),
		); 
	}

	protected function afterFind()
	{
		parent::afterFind();

		$postSubmition = PostSubmition::model();
		$postSubmition->post = $this;
		$this->postSubmition = $postSubmition->findByAttributes(array('post_id' => $this->id));

		if ($this->postSubmition !== null) {
			$author = new User;
			$author->name = $this->postSubmition->sender_name;
			$this->author = $author;
		}
	}

	protected function afterSave()
	{
		parent::afterSave();

		if ($this->notify && $this->published) 
			$this->notify(isset($this->notifyOptions[$this->notify]) ? $this->notify : ($this->isNewRecord ? 'postCreated' : 'postUpdated'));
	}

	protected function afterDelete()
	{
		parent::afterDelete();

		if ($this->published && $this->postSubmition !== null) {
			$this->postSubmition->notifySender('postSubmitionDeleted');
		}
	}

	public function getCategoryOptions()
	{
		$categories = Category::model()
			->post()
			->isChild()
			->findAll(array(
				'order' => 't.name',
			));

		return CHtml::listData($categories, 'id', 'name', 'parent.title');
	}

	public function getNotifyOptions()
	{
		return array(
			'postCreated' => 'Postagem Criada',
			'postUpdated' => 'Postagem Atualizada',
		);
	}

	public function notify($view)
	{
		if (!$this->private)
			return;

		$message = new YiiMailMessage;
		$message->setFrom(Yii::app()->params['mail.senderEmail']);
		$units = Unit::model()->findAll();

		foreach ($units as $unit) {
			$message->setTo(APP_ENVIRONMENT == 'local' ? Yii::app()->params['mail.senderEmail'] : $unit->email);
			$body = Yii::app()->controller->renderPartial(
				'common.views.mail.external.' . $view, 
				array('post' => $this, 'message' => $message), true
			);
			$message->setBody(array('message' => $message, 'content' => $body), 'text/html');
			Yii::app()->mail->send($message);
		}
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		if ($this->s) {
			$criteria->compare('t.title', $this->s, true);
			$criteria->compare('t.excerpt', $this->s, true, 'OR');							
		}

		$criteria->compare('t.private', $this->private);	

		return new CActiveDataProvider('Post', array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 't.published, t.publish_date DESC, t.title',
			),
		));
	}
}