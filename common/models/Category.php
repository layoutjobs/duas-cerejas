<?php

class Category extends CActiveRecord
{
	public $s;

	private $_ofRelation;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{category}}';
	}

	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name', 'unique'),
			array('parent_id, sequence', 'numerical', 'integerOnly' => true),
			array('name', 'length', 'max' => 128),
			array('description', 'length', 'max' => 256),
			array('img', 'file', 'types' => 'jpg, jpeg, gif, png', 'allowEmpty' => true),
		);
	}

	public function relations()
	{
		return array(
			'products' => array(self::MANY_MANY, 'Product', 'tbl_product_category(product_id, category_id)'),
			'posts' => array(self::MANY_MANY, 'Post', 'tbl_post_category(post_id, category_id)'),
			'categoryPosts' => array(self::HAS_MANY, 'PostCategory', 'category_id'),
			'parent' => array(self::BELONGS_TO, 'Category', 'parent_id'),
			// 'children' => array(self::HAS_MANY, 'Category', 'parent_id'),
		);
	}

	public function scopes()
	{
		$alias = $this->tableAlias;

		return array(
			'product' => array(
				'condition' => "$alias.belongs_to = 'Product'",
			),
			'post' => array(
				'condition' => "$alias.belongs_to = 'Post'",
			),
			'hasParent' => array(
				'condition' => "$alias.parent_id IS NULL OR $alias.parent_id = ''",
			),
			'isChild' => array(
				'condition' => "($alias.parent_id IS NOT NULL AND $alias.parent_id <> '')",
			),
		);
	}

	public function of($relation)
	{
		$this->_ofRelation = $relation;
		$relations = $this->relations();

		$belongsTo = explode(':', $relation);
		$belongsTo = $belongsTo[0];
		$belongsTo = $relations[$belongsTo][1];

		$this->getDbCriteria()->mergeWith(array(
			'condition' => "$this->tableAlias.belongs_to = '$belongsTo'",	
		));

		return $this;
	}

	public function notEmpty()
	{
		if ($this->_ofRelation === null)
			throw new CDbException('This method must be combinated with of().');

		$this->getDbCriteria()->mergeWith(array(
			'with' => array(
				$this->_ofRelation => array(
					'joinType' => 'INNER JOIN',
				),
			),			
		));

		return $this;
	}

	public function attributeLabels()
	{
		return array(
			'id' => '#',
			'parent_id' => 'Pai',
			'alias' => 'Alias',
			'name' => 'Nome',
			'description' => 'Descrição',
			'sequence' => 'Ordem',
			'img' => 'Imagem',	
		);
	}

	public function behaviors()
	{
		return array(
			'aliasBehavior' => array(
				'class' => 'common.behaviors.AliasBehavior',
			),
			'imageBehavior' => array(
				'class' => 'common.behaviors.ImageBehavior',
			),
		); 
	}

	public function getParentOptions()
	{
		$categories = Category::model()
			->hasParent()
			->findAll(array(
				'condition' => 't.belongs_to = :belongsTo',
				'params' => array(':belongsTo' => $this->belongs_to),
				'order' => 't.name',
			));

		return CHtml::listData($categories, 'id', 'name');
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('t.belongs_to', $this->belongs_to);

		if ($this->s) {
			$criteria->compare('t.name', $this->s, true);
			$criteria->compare('t.description', $this->s, true, 'OR');					
		}

		return new CActiveDataProvider('Category', array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'parent_id, sequence, name',
			),
		));
	}
}