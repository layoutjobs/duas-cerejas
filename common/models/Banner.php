<?php

class Banner extends CActiveRecord
{	
	public $s;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{banner}}';
	}

	public function rules()
	{
		return array(
			array('name, img, position, routes, active', 'required'),
			array('name, url, img', 'length', 'max' => 128),
			array('active', 'boolean'),
			array('sequence', 'numerical', 'integerOnly' => true),
			array('url', 'url'),
			array('img', 'file', 'types' => 'jpg, jpeg, gif, png', 'allowEmpty' => true),

			array('s', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
		);
	}

	public function scopes()
	{
		return array(
			'active' => array(
				'condition' => 'active = 1',
			),
			'asc' => array(
				'order' => 'sequence',
			),
			'desc' => array(
				'order' => 'sequence DESC',
			),
		);
	}

	public function attributeLabels()
	{
		return array(
			's' => 'Pesquisar',
			'id' => '#',
			'active' => 'Ativo',
			'name' => 'Nome',
			'sequence' => 'Ordem',
			'routes' => 'Páginas',
			'position' => 'Posições',
			'img' => 'Imagem',
		);
	}

	public function behaviors()
	{
		return array(
			'imageBehavior' => array(
				'class' => 'common.behaviors.ImageBehavior',
			),
			'positionBehavior' => array(
				'class' => 'common.behaviors.PositionBehavior',
			),
			'routeBehavior' => array(
				'class' => 'common.behaviors.RouteBehavior',
			),
		); 
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		if ($this->s) {
			$criteria->compare('name', $this->s, true);
			$criteria->compare('url', $this->s, true, 'OR');		
		}

		return new CActiveDataProvider('Banner', array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'position, active DESC, sequence, name',
			),
		));
	}
}