<?php

class Product extends CActiveRecord
{	
	public $s;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{product}}';
	}

	public function rules()
	{
		return array(
			array('name, categories, img, description, sweetness', 'required'),
			array('name', 'length', 'max' => 128),
			array('sweetness', 'in', 'range' => array(0, 2)),
			array('description', 'filter', 'filter' => array($obj = new CHtmlPurifier(), 'purify')),
			array('img', 'file', 'types' => 'jpg, jpeg, gif, png', 'allowEmpty' => true),

			array('s', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'categories' => array(self::MANY_MANY, 'Category', Yii::app()->db->tablePrefix . 'product_category(product_id, category_id)'),
			'productCategories' => array(self::HAS_MANY, 'ProductCategory', 'product_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			's' => 'Pesquisar',
			'id' => '#',
			'name' => 'Nome',
			'img' => 'Imagem',
			'description' => 'Descrição',
			'sweetness' => 'Doçura',
			'categories' => 'Categorias',
		);
	}

	public function behaviors()
	{
		return array(
			'saveRelatedBehavior' => array(
				'class' => 'common.behaviors.ESaveRelatedBehavior',
			),
			'imageBehavior' => array(
				'class' => 'common.behaviors.ImageBehavior',
			),
		); 
	}

	public function getCategoryOptions()
	{
		$categories = Category::model()
			->product()
			->isChild()
			->findAll(array(
				'order' => 't.name',
			));

		return CHtml::listData($categories, 'id', 'name', 'parent.name');
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		if ($this->s) {
			$criteria->compare('t.name', $this->s, true);
			$criteria->compare('t.description', $this->s, true, 'OR');							
		}

		return new CActiveDataProvider('Product', array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'name',
			),
		));
	}
}