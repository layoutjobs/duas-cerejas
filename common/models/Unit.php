<?php

class Unit extends CActiveRecord
{	
	public $s;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{unit}}';
	}

	public function rules()
	{
		return array(
			array('name, city, img, about, phone, address, email', 'required'),
			array('name, city, address, phone, email', 'length', 'max' => 128),
			array('about', 'filter', 'filter' => array($obj = new CHtmlPurifier(), 'purify')),
			array('img', 'file', 'types' => 'jpg, jpeg, gif, png', 'allowEmpty' => true),

			array('s', 'safe', 'on' => 'search'),
		);
	}

	public function relations()
	{
		return array(
			'user' => array(self::HAS_ONE, 'User', 'owner_id', 'condition' => 'type = :type', 'params' => array(':type' => 'unit')),
		);
	}

	public function attributeLabels()
	{
		return array(
			's' => 'Pesquisar',
			'id' => '#',
			'name' => 'Nome',
			'city' => 'Cidade',
			'img' => 'Imagem',
			'phone' => 'Telefone',
			'about' => 'Sobre',
			'address' => 'Endereço',
			'email' => 'E-mail',
			'user' => 'Usuário',
			'user.username' => 'Nome de Usuário',
		);
	}

	public function behaviors()
	{
		return array(
			'imageBehavior' => array(
				'class' => 'common.behaviors.ImageBehavior',
			),
		); 
	}

	public function getState()
	{
		return 'SP';
	}

	protected function afterValidate()
	{
		parent::afterValidate();

		if ($this->hasErrors())
			return;

		if ($this->user === null) {
			$password = explode('@', $this->email);
			$password = $password[0] . '123';
			$user = new User;
			$user->owner = $this;
			$user->newPassword = $password;
			$user->newPasswordRepeat = $user->newPassword;
		} else
			$user = $this->user;

		$user->name = $this->name;
		$user->email = $this->email;
		$user->username = $this->email;

		if (!$user->validate())	
			$this->addError('user', $this->getAttributeLabel('user') . ': ' . current(current($user->getErrors())));

		$this->user = $user;
	}

	protected function afterDelete()
	{
		parent::afterDelete();

		if ($this->user !== null)
			$this->user->delete();
	}

	protected function afterSave()
	{
		parent::afterSave();

		if ($this->user !== null) 
			$this->user->save(false);
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		if ($this->s) {
			$criteria->compare('name', $this->s, true);
			$criteria->compare('about', $this->s, true, 'OR');					
			$criteria->compare('address', $this->s, true, 'OR');			
			$criteria->compare('email', $this->s, true, 'OR');			
			$criteria->compare('city', $this->s, true, 'OR');			
		}

		return new CActiveDataProvider('Unit', array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'city, name',
			),
		));
	}
}