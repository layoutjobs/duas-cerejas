<?php

class User extends CActiveRecord
{
	public $s;
	public $newPassword;
	public $newPasswordRepeat;
	private $_notifyUser = false;
	private $_owner;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{user}}';
	}

	public function rules()
	{
		return array(
			array('name', 'required'),
			array('name, username, password, email', 'length', 'max' => 128),
			array('username, email', 'unique', 'message' => 'Este {attribute} já está sendo utilizado.'),
			array('email', 'email'),
			array('profile', 'safe'),
			array('type', 'in', 'range' => array_keys($this->typeOptions)),
			array('newPassword, newPasswordRepeat', 'length', 'min' => 6),
			array('newPassword, newPasswordRepeat', 'length', 'max' => 12),
			array('newPasswordRepeat', 'compare', 'compareAttribute' => 'newPassword', 'message' => '{attribute}: As senhas não correspondem.'),

			array('s', 'safe', 'on' => 'search'),
		);
	}

	public function attributeLabels()
	{
		return array(
			'id' => '#',
			'name' => 'Nome ou Apelido',
			'username' => 'Nome de Usuário',
			'password' => 'Senha',
			'email' => 'E-mail',
			'profile' => 'Perfil',
			'type' => 'Tipo',
			'newPassword' => 'Nova senha',
			'newPasswordRepeat' => 'Repita a nova senha',
		);
	}

	protected function afterFind()
	{
		parent::afterFind();

		if (($owner = $this->owner) !== null && $owner->email != $this->email)
			$owner->saveAttributes(array('email' => $this->email));
	}

	protected function beforeSave()
	{
		if (parent::beforeSave()) {
			$this->username = strtolower(trim($this->username));

			if ($this->isNewRecord) 
				$this->salt = $this->generateSalt();

			if ($this->newPassword === 'generate')
				$this->newPassword = $this->generatePassword();

			if ($this->newPassword)
				$this->password = $this->hashPassword($this->newPassword, $this->salt);	

			if ($this->owner !== null) {
				$this->owner_id = $this->owner->id;
				$this->type = lcfirst(get_class($this->owner));
			}

			return true;
		} else
			return false;
	}

	public function getOwner()
	{
		$id = $this->owner_id;
		$class = ucfirst($this->type);

		if ($id && $class && $this->_owner === null)
			$this->_owner = $class::model()->findByAttributes(array('id' => $id));

		return $this->_owner;
	}

	public function setOwner($owner)
	{
		$this->owner_id = $owner->id;
		$this->type = lcfirst(get_class($owner));
		$this->_owner = $owner;
	}

	public function getTypeOptions()
	{
		return array(
			'admin' => 'Admin',
			'unit' => 'Unidade',
		);
	}

	public function generatePassword()
	{
		return substr(hash('sha512', rand()), 0, 8);
	}

	public function sendMail($view)
	{	
		$message = new YiiMailMessage;
		$message->view = 'main';
		$body = Yii::app()->controller->renderPartial('common.views.mail.' . $view, array('user' => $this, 'message' => $message), true);
		$message->setBody(array('message' => $message, 'content' => $body), 'text/html');
		$message->setTo($this->email);
		$message->setFrom(Yii::app()->params['mail.from']);	
		return Yii::app()->mail->send($message);	
	}

	public function validatePassword($password)
	{
		return crypt($password, $this->password)===$this->password;
	}

	public function hashPassword($password)
	{
		return crypt($password, $this->generateSalt());
	}

	protected function generateSalt($cost = 10)
	{
		if (!is_numeric($cost) || $cost < 4 || $cost > 31)
			throw new CException(Yii::t('Cost parameter must be between 4 and 31.'));

		$rand = '';

		for ($i = 0; $i < 8; ++$i)
			$rand .= pack('S', mt_rand(0,0xffff));
		
		$rand .= microtime();
		$rand  = sha1($rand, true);
		$salt  = '$2a$' . str_pad((int) $cost, 2, '0', STR_PAD_RIGHT) . '$';
		$salt .= strtr(substr(base64_encode($rand), 0, 22), array('+' => '.'));
		return $salt;
	}

	public function search()
	{
		$criteria = new CDbCriteria;

		if ($this->s) {
			$criteria->compare('name', $this->s, true, 'OR');
			$criteria->compare('username', $this->s, true, 'OR');
			$criteria->compare('email', $this->s, true, 'OR');
		}

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 'name',
			),
		));
	}
}