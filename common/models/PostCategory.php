<?php

class PostCategory extends CActiveRecord
{
	public function tableName()
	{
		return '{{post_category}}';
	}

	public function relations()
	{
		return array(
		    'post'     => array(self::BELONGS_TO, 'Post', 'post_id'),
		    'category' => array(self::BELONGS_TO, 'Category', 'category_id'),
		);
	}
}