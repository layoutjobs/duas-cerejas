<?php

class PostSubmition extends CActiveRecord
{	
	public $s;
	public $published = false;
	public $publish_date;
	public $excerpt;
	public $categories;
	public $notify_sender = true;

	private $_notifySenderView;

	const STATUS_PENDING = 0;
	const STATUS_APPROVED = 1;
	const STATUS_REPROVED = 2;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return '{{post_submition}}';
	}

	public function rules()
	{
		return array(
			array('title, sender_name, sender_email, content', 'required'),
			array('title, sender_name, sender_email', 'length', 'max' => 128),
			array('info', 'length', 'max' => 400),
			array('notify_sender', 'boolean'),
			array('title', 'unique'),
			array('status', 'default', 'value' => self::STATUS_PENDING, 'setOnEmpty' => true),
			array('sender_email', 'email'),
			array('content', 'filter', 'filter' => array($purifier = new CHtmlPurifier(), 'purify')),
			array('date, publish_date', 'date', 'format' => Yii::app()->locale->dateFormat),
			array('date', 'default', 'value' => $this->formatDateTime('date', null), 'setOnEmpty' => true),
			array('post_id, sender_friends_emails', 'safe'),
			array('s', 'safe', 'on' => 'search'),
			array('publish_date, categories', 'required', 'on' => 'approval'),
		);
	}

	public function relations()
	{
		return array(
			'post' => array(self::BELONGS_TO, 'Post', 'post_id'),
		);
	}

	public function attributeLabels()
	{
		return array(
			's' => 'Pesquisar',
			'id' => '#',
			'title' => 'Título',
			'content' => 'Conteúdo',
			'excerpt' => 'Resumo',
			'sender_name' => 'Nome',
			'sender_email' => 'E-mail',
			'sender_friends_emails' => 'E-mails Amigos',
			'date' => 'Data',
			'upload_folder' => 'Pasta Upload',
			'categories' => 'Categorias',
			'published' => 'Publicado?',
			'publish_date' => 'Data Publicação',
			'notify_sender' => 'Notificar usuário e amigos',
		);
	}

	public function behaviors()
	{
		return array(
			'saveRelatedBehavior' => array(
				'class' => 'common.behaviors.ESaveRelatedBehavior',
			),
			'excerptBehavior' => array(
				'class' => 'common.behaviors.ExcerptBehavior',
			),
			'dateTimeBehavior' => array(
				'class' => 'common.behaviors.DateTimeBehavior',
			),
			'fileBehavior' => array(
				'class' => 'common.behaviors.FileBehavior',
				'uploadFolder' => 'tmp',
				'uploadModelFolder' => $this->upload_folder ? $this->upload_folder : substr(md5($this->primaryKey), 0, 10),
				'uploadScopes' => array(
					'gallery' => array(
						'acceptFileTypes' => '/(\.|\/)(gif|jpe?g|png)$/i',
					),
				),
			),
		); 
	}

	protected function afterFind()
	{
		if (self::model()->scenario !== 'update')
			$this->scenario = self::model()->scenario;

		if (self::model()->post !== null)
			$this->post = self::model()->post;

		$this->published = $this->post !== null ? 1 : 0;
		$this->categories = $this->post !== null ? $this->post->categories : null;
		$this->publish_date = $this->post !== null ? $this->post->formatDateTime('publish_date') : $this->formatDateTime('date');

		parent::afterFind();
	}

	protected function afterValidate()
	{
		if (!$this->hasErrors()) {
			if ($this->status == self::STATUS_APPROVED) {
				if ($this->post === null)
					$post = new Post;
				else
					$post = $this->post;

				foreach ($this->safeAttributeNames as $attribute) {
					if ($post->isAttributeSafe($attribute))
						$post->{$attribute} = $this->{$attribute};
				}			

				$this->post = $post;
				$this->post_id = $post->id;
				$this->post->validate();

				if ($this->post->hasErrors())
					$this->addError('post_id', current(current($this->post->getErrors())));
			}
		}

		parent::afterValidate();
	}

	protected function beforeSave()
	{
		if (parent::beforeSave()) {
			if (!$this->_notifySenderView) {
				switch ($this->status) {
					case self::STATUS_PENDING:
						$this->_notifySenderView = $this->isNewRecord ? 'postSubmitionCreated' : 'postSubmitionUpdated';
						break;
					case self::STATUS_APPROVED:
						$this->_notifySenderView = 'postSubmitionApproved';
						break;
					case self::STATUS_REPROVED:
						$this->_notifySenderView = 'postSubmitionReproved';	
						break;												
				}
			}

			return true;
		} else
			return false;

		if ($this->notify_sender && $this->_notifySenderView)
			$this->notifySender($this->_notifySenderView);

		parent::afterSave();
	}


	protected function afterSave()
	{
		if ($this->status == self::STATUS_APPROVED && $this->post !== null) {
			$this->post->saveWithRelated(array('categories'));

			if ($this->post_id == null)
				$this->saveAttributes(array('post_id' => $this->post->id));

			$this->post = Post::model()->findByPk($this->post_id);

			if (is_dir($this->uploadPath)) {
				CFileHelper::copyDirectory($this->uploadPath, $this->post->uploadPath);
				CFileHelper::removeDirectory($this->uploadPath);
			}
		} elseif ($this->post !== null) {
			if (is_dir($this->post->uploadPath)) {
				CFileHelper::copyDirectory($this->post->uploadPath, $this->uploadPath);
				CFileHelper::removeDirectory($this->post->uploadPath);
			}

			$this->post->delete();
		} 

		if ($this->notify_sender && $this->_notifySenderView)
			$this->notifySender($this->_notifySenderView);

		parent::afterSave();
	}

	protected function afterDelete()
	{
		if ($this->post !== null) {
			$this->post->delete();
		}

		parent::afterDelete();
	}

	public function getStatusOptions()
	{
		return array(
			self::STATUS_PENDING => 'Pendente',
			self::STATUS_APPROVED => 'Aprovado',
			self::STATUS_REPROVED => 'Reprovado',
		);
	}

	public function getCategoryOptions()
	{
		return Post::model()->categoryOptions;
	}

	public function notifySender($view)
	{
		$message = new YiiMailMessage;
		$message->setTo($this->sender_email);
		$message->setFrom(Yii::app()->params['mail.senderEmail']);

		if ($this->status == self::STATUS_APPROVED || $view == 'postSubmitionDeleted') {
			$message->setCc(array_filter(explode(',', $this->sender_friends_emails), 'trim')); 
		}

		$body = Yii::app()->controller->renderPartial(
			'common.views.mail.external.' . $view, 
			array('postSubmition' => $this, 'message' => $message), true
		);

		$message->setBody(array('message' => $message, 'content' => $body), 'text/html');
		return Yii::app()->mail->send($message);
	}

	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->condition = 't.post_id IS NULL';

		if ($this->s) {
			$criteria->compare('t.title', $this->s, true);
			$criteria->compare('t.sender_name', $this->s, true, 'OR');						
			$criteria->compare('t.content', $this->s, true, 'OR');						
		}

		// $criteria->order = '(post_id IS NULL)';

		return new CActiveDataProvider('PostSubmition', array(
			'criteria' => $criteria,
			'sort' => array(
				'defaultOrder' => 't.date, t.title',
			),
		));
	}
}