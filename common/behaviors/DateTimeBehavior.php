<?php

/*
 * DateTimeBehavior
 * Automatically converts date and datetime fields.
 * 
 */
class DateTimeBehavior  extends CActiveRecordBehavior
{
	/*
	 * Formato de saída do banco de dados no formato do PHP.
	 */
	public $dateOutcomeFormat = 'Y-m-d';
	public $dateTimeOutcomeFormat = 'Y-m-d H:i:s';

	/*
	 * Formato de entrada do banco de dados no formato do Yii.
	 */
	public $dateIncomeFormat = 'yyyy-MM-dd';
	public $dateTimeIncomeFormat = 'yyyy-MM-dd hh:mm:ss';

	public function beforeSave($event)
	{
		
		//search for date/datetime columns. Convert it to pure PHP date format
		foreach($event->sender->tableSchema->columns as $columnName => $column){
						
			if (($column->dbType != 'date') and ($column->dbType != 'datetime')) continue;
									
			if (!strlen($event->sender->$columnName)){ 
				$event->sender->$columnName = null;
				continue;
			}
			
			if (($column->dbType == 'date')) {				
				$event->sender->$columnName = date($this->dateOutcomeFormat, CDateTimeParser::parse($event->sender->$columnName, Yii::app()->locale->dateFormat));
			} else {	
				$event->sender->$columnName = date($this->dateTimeOutcomeFormat, 
					CDateTimeParser::parse($event->sender->$columnName, 
						strtr(Yii::app()->locale->dateTimeFormat, 
							array("{0}" => Yii::app()->locale->timeFormat,
								  "{1}" => Yii::app()->locale->dateFormat))));
			}			
			
		}

		return true;
	}
	
	public function afterFind($event)
	{				
		foreach($event->sender->tableSchema->columns as $columnName => $column){
						
			if (($column->dbType != 'date') and ($column->dbType != 'datetime')) continue;
			
			if (!strlen($event->sender->$columnName)){ 
				$event->sender->$columnName = null;
				continue;
			}
			
			if ($column->dbType == 'date'){				
				$event->sender->$columnName = Yii::app()->dateFormatter->formatDateTime(
					CDateTimeParser::parse($event->sender->$columnName, $this->dateIncomeFormat),'medium',null);
			} else {				
				$event->sender->$columnName = 
					Yii::app()->dateFormatter->formatDateTime(
							CDateTimeParser::parse($event->sender->$columnName,	$this->dateTimeIncomeFormat), 
							'medium', 'medium');
			}
		}
		return true;
	}
	
	
	public function formatDateTime($attr = null, $value = null, $dbType = 'date')
	{
		$value = !$value ? $this->owner->$attr : $value;
		$dbType = $attr ? $this->owner->tableSchema->columns[$attr]->dbType : $dbType;
		
		if ($dbType == 'date') {				
			return Yii::app()->dateFormatter->formatDateTime(
				CDateTimeParser::parse($value, $this->dateIncomeFormat), 'medium', null);
		} elseif ($dbType == 'datetime') {				
			return Yii::app()->dateFormatter->formatDateTime(
				CDateTimeParser::parse($value, $this->dateTimeIncomeFormat), 'medium', 'medium');
		} else
			return $value;	
	}
	
    public function parseDateTime($attr = null, $value = null, $dbType = 'date')
    {
		$value = !$value ? $this->owner->$attr : $value;
		$dbType = $attr ? $this->owner->tableSchema->columns[$attr]->dbType : $dbType;    	
		
		if (($dbType == 'date')) {				
			$value = date($this->dateOutcomeFormat, CDateTimeParser::parse($value, Yii::app()->locale->dateFormat));
		} else {	
			$value = date($this->dateTimeOutcomeFormat, 
				CDateTimeParser::parse($value, 
					strtr(Yii::app()->locale->dateTimeFormat, 
						array("{0}" => Yii::app()->locale->timeFormat,
							  "{1}" => Yii::app()->locale->dateFormat))));
		}		
		
		return $value;
    }
}