<?php

class RouteBehavior extends CActiveRecordBehavior
{
	public $routeAttribute = 'routes';

	public function beforeSave($event)
	{
		$this->owner->{$this->routeAttribute} = $this->parseRoute($this->owner->{$this->routeAttribute});
	}

	public function parseRoute($route)
	{
		if (is_array($route))
			return implode(', ', $route);	
		else
			return $route;	
	}

	public function getRouteAsArray()
	{
		if (!is_array($this->owner->{$this->routeAttribute})) {
			$routes = explode(',', $this->owner->{$this->routeAttribute});
			$routes = array_map('trim', $routes);
			return $routes;	
		}
	}

	public function getRouteAsOptionsLabels()
	{
		$routes = array_flip($this->routeAsArray);
		$options = $this->routeOptions;

		foreach ($options as $option => $label) {
			if (!isset($routes[$option]))
				unset($options[$option]);
		}

		return implode(', ', $options);
	}

	public function getRouteOptions()
	{
		return Yii::app()->params['site.routes'];	
	}

	public function route($route)
	{	
		$parts = explode('/', $route);
		$alias = $this->owner->tableAlias;
		$table = $this->owner->tableName();
		$this->owner->dbCriteria->mergeWith(array(
			//'condition' => "($alias.$this->routeAttribute REGEXP ',?$route,?') OR ((SELECT COUNT($table.$this->routeAttribute) FROM $table WHERE $table.$this->routeAttribute REGEXP ',?$route,?') = 0 AND $alias.$this->routeAttribute REGEXP ',?$parts[0],?')",	
			'condition' => "FIND_IN_SET('$route', REPLACE($alias.$this->routeAttribute, ', ', ',')) OR (FIND_IN_SET('$parts[0]', REPLACE($alias.$this->routeAttribute, ', ', ',')) AND (SELECT COUNT($this->routeAttribute) FROM $table WHERE FIND_IN_SET('$route', REPLACE($this->routeAttribute, ', ', ','))) = 0)",	
		));

		return $this->owner;
	}
}
