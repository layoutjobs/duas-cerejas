<?php

class ExcerptBehavior extends CActiveRecordBehavior
{
	public $excerptAttribute = 'excerpt';
	public $excerptOriginAttribute = 'content';
	public $excerptLength = 600;

	private $_oldExcerpt = null;

	public function afterFind($event)
	{
		$this->_oldExcerpt = $this->createExcerpt($this->owner->{$this->excerptOriginAttribute}, $this->excerptLength);

		if ($this->owner->{$this->excerptAttribute})
			return;
		else
			$this->owner->{$this->excerptAttribute} = $this->_oldExcerpt;
	}

	public function beforeSave($event)
	{
		$newExcerpt = $this->createExcerpt($this->owner->{$this->excerptOriginAttribute}, $this->excerptLength);

		if (!$this->owner->{$this->excerptAttribute} || $this->owner->{$this->excerptAttribute} == $this->_oldExcerpt)
			$this->owner->{$this->excerptAttribute} = $newExcerpt;

		$this->_oldExcerpt = null;
	}

	public function createExcerpt($origin, $length)
	{
		$purifier = new CHtmlPurifier();
		$purifier->options = array('HTML.Allowed' => '');
		$text = $purifier->purify($origin);
		$text = preg_replace('!\s+!', ' ', $text);
		$text = trim($text);
		$text = trim($text, '.');
		$text = trim($text, ',');

		if (strlen($text) <= $length)
			return $text;
		else {
			$text = substr($text, 0, ($length - 3));
			$text = trim($text);
			return $text . '...';
		}
	}
}
