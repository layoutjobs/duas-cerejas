<?php

class ImageBehavior extends CActiveRecordBehavior
{
	private $_oldImg;

	public function afterFind($event)
	{
		$this->_oldImg = $this->owner->img;
	}

	public function beforeSave($event)
	{
		$this->saveImage();
	}

	public function afterDelete($event)
	{
		$this->deleteImage();
	}

	public function getImage($version = null)
	{
		if (!$this->owner->img)
			return;

		$url = APP_FILE_URL . '/' . (strtolower(get_class($this->owner))) . ($version !== null ? '/' . $version : null) . '/' . $this->owner->img;

		return new CAttributeCollection(array(
			'name' => $this->owner->img,
			'url' => $url,
		));
	}

	public function saveImage()
	{
		if ($this->owner->img && $this->owner->img instanceof CUploadedFile) {
			$this->deleteImage();
			$imgName = uniqid() . '.' . $this->owner->img->extensionName;
			$basePath = APP_FILE_PATH . DIRECTORY_SEPARATOR . strtolower(get_class($this->owner));
			$fullPath = $basePath . DIRECTORY_SEPARATOR . $imgName;
			$thumbnailPath = $basePath . DIRECTORY_SEPARATOR . 'thumbnail' . DIRECTORY_SEPARATOR . $imgName;
			$this->owner->img->saveAs($fullPath);
			$this->owner->img = $imgName;				
			$thumbnail = Yii::app()->image->load($fullPath);
			$thumbnail->resize(300, 300);
			$thumbnail->save($thumbnailPath);
		}
	}

	public function deleteImage()
	{
		$basePath = APP_FILE_PATH . DIRECTORY_SEPARATOR . strtolower(get_class($this->owner));
		$fullPath = $basePath . DIRECTORY_SEPARATOR . $this->_oldImg;
		$thumbnailPath = $basePath . DIRECTORY_SEPARATOR . 'thumbnail' . DIRECTORY_SEPARATOR . $this->_oldImg;
		if (is_file($fullPath)) unlink($fullPath);
		if (is_file($thumbnailPath)) unlink($thumbnailPath);
	}
}
