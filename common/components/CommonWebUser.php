<?php

class CommonWebUser extends CWebUser
{
	const ALERT_SUCCESS = 'success';
	const ALERT_INFO = 'info';
	const ALERT_WARNING = 'warning';
	const ALERT_DANGER = 'danger';

    public function is($what)
    {
    	if ($what === 'guest')
    		return $this->isGuest;
    	else
    		return $this->getState('is' . ucfirst($what), false);
    }

	public function setAlert($type, $message)
	{
		if (isset($_POST['ajax']))
			return null;

		$this->setFlash($type, $message);
	}

	public function getAlert($type)
	{
		return $this->getFlash($type, false);
	}
}