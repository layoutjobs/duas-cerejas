<?php

$params = require(__DIR__ . '/params-local.php');

return array(
	'components' => array(
		'db' => array(
			'connectionString' => 'mysql:host=localhost;dbname=duascerejas',
			'username' => 'root',
			'password' => '',
		),
	),
	'params' => $params,
);