<?php

return array(
	'site.name' => 'Duas Cerejas',
	'site.keywords' => 'bolo, bolo de chocolate, bolo de casamento, bolo de aniversário, bolo de frutas, bolo trufado, bolos artísticos, bolo personalizado, bolo de criança, bolo Itu, bolo Indaiatuba, bolo para festa, bolos e Doces, bolo gostoso, bolo caseiro, bolo de nozes, bolo para eventos, bolo para comemorações, duas cerejas',
	'site.description' => 'Duas Cerejas, grande variedade de bolos para festas, aniversários, e qualquer outra ocasião. A melhor opção de bolos em Itu, Indaiatuba e região.',
	'site.positions' => array(
		'banner-a' => 'Banner Principal',
		'banner-b' => 'Banner Promocional',
	),
	'site.routes' => array(
		'home' => 'Home',
		'home/about' => 'Empresa',
		'home/franchise' => 'Franquia',
		'product' => 'Produtos',
		'unit' => 'Unidades',
		'post' => 'Blog',
	),
	'mail.senderEmail' => null,
	'franchise.email' => 'franquia@duascerejas.com.br',
	'facebook.url' => 'https://www.facebook.com/pages/duascerejas/655537047871534',
	'ga.trackingId' => 'UA-56405285-1',
	'upload.imageVersions' => array(
		'thumbnail' => array(
			'max_width' => 240,
			'max_height' => 160,
		),
		'medium' => array(
			'max_width' => 640,
			'max_height' => 480,
		),
		'large' => array(
			'max_width' => 800,
			'max_height' => 600,
		),
	),
);