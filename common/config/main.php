<?php

$environment = APP_ENVIRONMENT;
$params = require(__DIR__ . '/params.php');

return CMap::mergeArray(array(
	'name'              => $params['site.name'],
	'id'                => 'app',
	'defaultController' => 'home', 
	'language'          => 'pt_br',
	'timeZone'          => 'America/Sao_Paulo', 

	'aliases' => array(
		'common' => $common,
		'mail' => 'common.extensions.mail',
    ),

	'preload' => array(
		'log',
	),

	'import' => array(
        'common.components.*',
        'common.extensions.*',
        'common.models.*',
        'application.components.*',
        'application.extensions.*',
        'application.models.*',
		'mail.YiiMailMessage',
	),

	'components' => array(
		'coreMessages' => array(
			'basePath' => $common . '/messages', 
		),
		'user' => array(
			'class' => 'WebUser',
			'loginUrl' => array('home/login'),
			'allowAutoLogin' => true,
		),
		'db' => array(
			'emulatePrepare' => true,
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_',
		),
		'errorHandler' => array(
			'errorAction' => 'home/error',
		),
		'log' => array(
			'class' => 'CLogRouter',
			'routes' => array(
				array(
					'class' => 'CFileLogRoute',
					'levels' => 'error, warning',
				),
			),
		),
		'mail' => array(
 			'class' => 'mail.YiiMail',
 			'viewPath' => 'common.views.layouts.mail',
 			'logging' => true,
 			'dryRun' => false,
		),
		'image' => array(
			'class' => 'common.components.CommonImage',
			'librarySourcePath' => 'common.vendor.image',
		),
		'clientScript' => array(
			'packages' => array(
				'jquery' => array(
					'baseUrl' => APP_BASE_URL,
					'js' => array(
						'js/jquery.js'
					),
				),
				'owl.carousel' => array(
					'baseUrl' => APP_BASE_URL,
					'css' => array(
						'css/owl.carousel.css',
					),
					'js' => array(
						'js/owl.carousel.js',
					),
					'depends' => array('jquery'),
				),
				'prettyPhoto' => array(
					'baseUrl' => APP_BASE_URL,
					'css' => array(
						'vendor/prettyPhoto/css/prettyPhoto.css',
					),
					'js' => array(
						'vendor/prettyPhoto/js/jquery.prettyPhoto.js',
					),
					'depends' => array('jquery'),
				),
			),
		),
	),

	'params' => $params,
), require(__DIR__ . '/main-' . $environment . '.php'));