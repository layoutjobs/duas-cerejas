<?php 
$message->subject = 'Nova Publicação: ' . CHtml::encode($post->title); 
$message->view = 'external'; 
?>

<p>Olá.</p>

<p>Uma nova publicação foi criada. Por favor, acesse nosso site para conferir detalhes.</p>

<p><b><?php echo CHtml::encode($post->title) ; ?></b></p>

<p>Obrigado!</p>