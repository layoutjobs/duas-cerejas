<?php 
$message->subject = 'Publicação Removida: ' . CHtml::encode($postSubmition->title); 
$message->view = 'external'; 
?>

<p>Olá.</p>

<p>A publicação <b><?php echo CHtml::encode($postSubmition->title) ; ?></b> foi removida do nosso site.</p>

<p>Obrigado!</p>