<?php 
$message->subject = 'Publicação Atualizada: ' . CHtml::encode($post->title); 
$message->view = 'external'; 
?>

<p>Olá.</p>

<p>A publicação abaixo foi atualizada. Por favor, acesse nosso site para conferir detalhes.</p>

<p><b><?php echo CHtml::encode($post->title) ; ?></b></p>

<p>Obrigado!</p>