<?php 
$message->subject = 'Envio de Publicação ' . CHtml::encode($postSubmition->title); 
$message->view = 'external'; 
?>

<p>Olá <b><?php echo CHtml::encode($postSubmition->sender_name) ; ?></b>.</p>

<p>A publicação <b><?php echo CHtml::encode($postSubmition->title) ; ?></b> foi atualizada. Por favor, acesse nosso site para conferir as mudanças.</p>

<?php if ($postSubmition->info) : ?>

<p><b>Info:</b> <?php echo CHtml::encode($postSubmition->info); ?></p>

<?php endif; ?>

<p>Obrigado!</p>

<p>* Publicação enviada por: <b><?php echo CHtml::encode($postSubmition->sender_name) ; ?></b></p>