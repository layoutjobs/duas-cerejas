<?php 
$message->subject = 'Envio de Publicação ' . CHtml::encode($postSubmition->title); 
$message->view = 'external'; 
?>

<p>Olá <b><?php echo CHtml::encode($postSubmition->sender_name) ; ?></b>.</p>

<p>Recebemos o seu pedido de publicação <b><?php echo CHtml::encode($postSubmition->title) ; ?></b>. No momento, estamos analisando o conteúdo e em breve você receberá um novo e-mail notificando se o mesmo foi publicado.</p>

<p>Após a publicação, os seguintes e-mails também receberão um aviso: <?php echo CHtml::encode($postSubmition->sender_friends_emails) ; ?>.</p>

<?php if ($postSubmition->info) : ?>

<p><b>Info:</b> <?php echo CHtml::encode($postSubmition->info); ?></p>

<?php endif; ?>

<p>Obrigado!</p>