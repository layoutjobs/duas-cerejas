<?php 
$message->subject = 'Envio de Publicação ' . CHtml::encode($postSubmition->title); 
$message->view = 'external'; 
?>

<p>Olá.</p>

<p>A publicação <b><?php echo CHtml::encode($postSubmition->title) ; ?></b> foi aprovada. Por favor, acesse nosso site para conferir a publicação.</p>

<?php if ($postSubmition->info) : ?>

<p><b>Info:</b> <?php echo CHtml::encode($postSubmition->info); ?></p>

<?php endif; ?>

<p>Obrigado!</p>

<p>* Publicação enviada por: <b><?php echo CHtml::encode($postSubmition->sender_name) ; ?></b></p>