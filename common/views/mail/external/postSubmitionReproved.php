<?php 
$message->subject = 'Envio de Publicação ' . CHtml::encode($postSubmition->title); 
$message->view = 'external'; 
?>

<p>Olá <b><?php echo CHtml::encode($postSubmition->sender_name) ; ?></b>.</p>

<p>Infelizmente sua publicação <b><?php echo CHtml::encode($postSubmition->title) ; ?></b> não foi aprovada.</p>

<?php if ($postSubmition->info) : ?>

<p><b>Info:</b> <?php echo CHtml::encode($postSubmition->info); ?></p>

<?php endif; ?>

<p>Obrigado!</p>