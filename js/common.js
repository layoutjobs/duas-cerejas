!function($) {

	"use strict";

	$('html').addClass('js');
	$('html').removeClass('no-js');

	var hasFlash = false;

	try {
		var fo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
		if (fo) {
			hasFlash = true;
		}
	} catch (e) {
		if (navigator.mimeTypes
			&& navigator.mimeTypes['application/x-shockwave-flash'] != undefined
			&& navigator.mimeTypes['application/x-shockwave-flash'].enabledPlugin) {
				hasFlash = true;
		}
	}

	if (hasFlash) {
		$('html').addClass('swf');
		$('html').removeClass('no-swf');
	}

}(window.jQuery);