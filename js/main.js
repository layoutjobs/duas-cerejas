!function($) {

  	"use strict";

	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});

}(window.jQuery);